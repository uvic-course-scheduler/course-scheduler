# scheduler-frontend

This readme includes lots of information on how to effectively contribute to the course scheduler website.

## Setting up your development environment

### Installing software

Install the following software:

* VSCode
* WSL
  * NVM

If you're working on windows, work from within WSL. This will allow you to build the app in an environment which is more consistent with the automated CI builds. The following instructions expect you to be working from bash or zsh.

### Cloning the repository

First, create a new SSH key and add it to your GitLab account. This will authorize you to clone and edit our private repositories. Then, clone the repository. You should also configure git to use your name and email.

```bash
git clone git@gitlab.com:uvic-course-scheduler/scheduler-frontend.git
```

The recommended editor for this project is VSCode with the following extensions:

* "Debugger for Chrome"
* "ESLint"
* "Prettier - Code formatter"
* "markdownlint"

## Making code changes

The following instructions are based on the Git CLI, but you are welcome to use a standalone Git GUI like [fork](https://git-fork.com/), [Github Desktop](https://desktop.github.com/), or [Sourcetree](https://www.sourcetreeapp.com/). The source control features in VSCode are pretty good, too.

You should NEVER commit directly to the *master* or *develop* branches unless you have a really good reason to. All development work should be done on your own branch. To make such a branch, do the following:

```bash
# checkout develop and pull the most recent version
git checkout develop
git pull

# create a new feature branch off of develop
git checkout -b 'ISSUENUM_short-issue-description'
```

As you work on your issue, you should be regularily staging changes and committing them to your branch with [good commit messages](https://chris.beams.io/posts/git-commit/).

## Code style

We are using several tools to keep our coding style consistent.

* esLint
* prettier
* markdownlint


## libraries 
- https://www.npmjs.com/package/react-color
- https://codesandbox.io/s/material-ui-sortable-list-with-react-smooth-dnd-forked-gbwbd?file=/src/index.js
