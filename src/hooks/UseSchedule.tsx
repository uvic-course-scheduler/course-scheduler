import API from "@aws-amplify/api";
import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";
import "@fullcalendar/timegrid/main.css";
import { Schedule } from "API";
import { Auth, graphqlOperation } from "aws-amplify";
import { useEffect, useState } from "react";
import { getCourseMapForSchedule, getScheduleById } from "utils/APICalls";
import Observable from "zen-observable-ts";
import { onUpdateSchedule } from "../graphql/subscriptions";
import CourseMap from "./CourseMap";
import ManagedSchedule from "./ManagedSchedule";

// hook for setting up a subscription to a schedule and it's associated courses, does not require
// the schedule to be owned by the currently logged in user.
function useManagedSchedule(scheduleId: string): ManagedSchedule | null {
  const [schedule, setSchedule] = useState<Schedule | null>(null);
  const [courses, setCourses] = useState<Promise<CourseMap> | null>(null);

  // keep track of the schedule
  useEffect(() => {
    // get initial schedule (otherwise schedule would not show up until subscription update)
    getScheduleById(scheduleId).then((schedule) => {
      if (schedule) setSchedule(schedule);
    });

    // subscribe to schedule updates
    // (see: https://docs.amplify.aws/lib/graphqlapi/subscribe-data/q/platform/js)
    // TODO: pretty sure this only works for schedules owned by the current logged in user
    const subscription = Auth.currentAuthenticatedUser().then((user) => {
      return (API.graphql(
        graphqlOperation(onUpdateSchedule, { owner: user.username })
      ) as Observable<object>).subscribe({
        next: (value: any) => {
          const updatedSchedule: Schedule = value.value.data.onUpdateSchedule;
          if (updatedSchedule.id === scheduleId) {
            setSchedule(updatedSchedule);
          }
        },
        error: (error) => {
          console.error("Subscription error", error);
        },
      });
    });

    // Cleanup the subscription when the component unmounts
    return () => {
      subscription.then((subscription) => subscription.unsubscribe());
    };
  }, [scheduleId]);

  // when the schedule is updated, update course information for courses in that schedule
  useEffect(() => {
    if (!schedule) return;
    setCourses(getCourseMapForSchedule(schedule));
  }, [schedule]);

  return schedule && courses ? { schedule, courses } : null;
}

function useSchedule(scheduleId: string): Schedule | null {
  const managedSchedule = useManagedSchedule(scheduleId);

  if (managedSchedule) {
    return managedSchedule.schedule;
  }

  return null;
}

function useCourses(scheduleId: string): CourseMap | null {
  const managedSchedule = useManagedSchedule(scheduleId);
  const [
    coursesForSchedule,
    setCoursesForSchedule,
  ] = useState<CourseMap | null>(null);

  useEffect(() => {
    if (!managedSchedule) return;
    managedSchedule.courses.then((courses) => {
      setCoursesForSchedule(courses);
    });
  }, [managedSchedule]);

  return coursesForSchedule;
}

export { useManagedSchedule, useSchedule, useCourses };
