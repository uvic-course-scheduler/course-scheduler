import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";
import "@fullcalendar/timegrid/main.css";
import { Schedule } from "API";
import React, { createContext, useContext, useEffect, useState } from "react";
import {
  getCourseMapForSchedule,
  getScheduleCreateSubscriptionForUser,
  getScheduleDeleteSubscriptionForUser,
  getSchedulesForUser,
  getScheduleUpdateSubscriptionForUser,
} from "utils/APICalls";
import CourseMap from "./CourseMap";
import ManagedSchedule from "./ManagedSchedule";
import { useAuth } from "./UseAuth";

enum ScheduleCacheStatus {
  UNLOADED = "Unloaded",
  LOADING = "Loading",
  LOADED = "Loaded",
}

interface ScheduleMap {
  [scheduleId: string]: ManagedSchedule;
}

/**
 * Models an object used to keep track of a user's schedules
 */
interface ScheduleCache {
  // whether the schedule cache has loaded yet
  status: ScheduleCacheStatus;

  // object mapping schedule Ids to managed schedule objects
  cachedSchedules: ScheduleMap;
}

interface ScheduleCacheSubscriptions {
  updateSubscription: ZenObservable.Subscription;

  createSubscription: ZenObservable.Subscription;

  deleteSubscription: ZenObservable.Subscription;
}

function useProvideCachedSchedules(): ScheduleCache {
  const [cachedSchedules, setCachedSchedules] = useState<ScheduleMap>({});
  const [
    scheduleSubscriptions,
    setScheduleSubscriptions,
  ] = useState<ScheduleCacheSubscriptions | null>(null);
  const [cacheStatus, setCacheStatus] = useState(ScheduleCacheStatus.UNLOADED);
  const auth = useAuth();

  // when a new user logs in, load their schedules and set up subscriptions
  useEffect(() => {
    if (
      cacheStatus === ScheduleCacheStatus.LOADED ||
      cacheStatus === ScheduleCacheStatus.LOADING ||
      auth.authPending ||
      !auth.user
    ) {
      return;
    }

    setCacheStatus(ScheduleCacheStatus.LOADING);

    // set up an inital state for the cache with all of the user's schedules
    getSchedulesForUser(auth.user.username).then((schedules: Schedule[]) => {
      let initialCache: { [key: string]: ManagedSchedule } = {};

      schedules.forEach((schedule) => {
        // skip invalid schedules
        if (!schedule.id) {
          console.error("Invalid schedule (missing ID)", schedule);
          return;
        }

        // add the schedule to the cache
        initialCache[schedule.id] = {
          schedule: schedule,
          courses: getCourseMapForSchedule(schedule),
        };
      });
      setCachedSchedules(initialCache);
      setCacheStatus(ScheduleCacheStatus.LOADED);
    });

    async function updateScheduleCache(
      updatedSchedule: Schedule,
      deleted = false
    ) {
      if (deleted) {
        setCachedSchedules((prevCacheState) => {
          let updatedScheduleCache = { ...prevCacheState };
          if (updatedSchedule.id)
            delete updatedScheduleCache[updatedSchedule.id];
          return updatedScheduleCache;
        });
      } else {
        setCachedSchedules((prevCacheState) => {
          if (!updatedSchedule.id) {
            console.error("Updated schedule missing ID", updatedSchedule);
            return prevCacheState;
          }
          let updatedScheduleCache = {
            ...prevCacheState,
            [updatedSchedule.id]: {
              schedule: updatedSchedule,
              courses: getCourseMapForSchedule(updatedSchedule),
            },
          };
          return updatedScheduleCache;
        });
      }
    }

    // listen for updates to schedules
    setScheduleSubscriptions({
      updateSubscription: getScheduleUpdateSubscriptionForUser(
        auth.user.username,
        (updatedSchedule: Schedule) => {
          updateScheduleCache(updatedSchedule);
        }
      ),
      createSubscription: getScheduleCreateSubscriptionForUser(
        auth.user.username,
        (createdSchedule: Schedule) => {
          updateScheduleCache(createdSchedule);
        }
      ),
      deleteSubscription: getScheduleDeleteSubscriptionForUser(
        auth.user.username,
        (deletedSchedule: Schedule) => {
          updateScheduleCache(deletedSchedule, true);
        }
      ),
    });
  }, [auth, cacheStatus, cachedSchedules]);

  useEffect(() => {
    // Cleanup the subscription when the component unmounts
    return () => {
      if (scheduleSubscriptions) {
        scheduleSubscriptions.createSubscription.unsubscribe();
        scheduleSubscriptions.updateSubscription.unsubscribe();
        scheduleSubscriptions.deleteSubscription.unsubscribe();
      }
    };
  }, [scheduleSubscriptions]);

  return {
    status: cacheStatus,
    cachedSchedules: cachedSchedules,
  };
}

// Schedule context to make available to childen of ProvideAuth
// (see: https://reactjs.org/docs/context.html) for more info on react contexts.
const CachedScheduleContext = createContext<ScheduleCache | null>(null);

/**
 * Provider component that wraps the app and makes a cached schedule object available to any
 * child component that calls useCachedSchedule().
 */
function ProvideCachedSchedules(props: { children: any }) {
  const cachedSchedules = useProvideCachedSchedules();
  return (
    <CachedScheduleContext.Provider value={cachedSchedules}>
      {props.children}
    </CachedScheduleContext.Provider>
  );
}

function useCachedSchedule(scheduleId: string): Schedule | null {
  const scheduleCache = useContext(CachedScheduleContext);

  // if the schedule cache has not yet been initialized
  if (!scheduleCache) {
    console.error(
      "Tried to use cached schedules without schedule cache context."
    );
    return null;
  }

  // if the schedule does not yet exist in the cache return null
  if (!scheduleCache.cachedSchedules.hasOwnProperty(scheduleId)) {
    return null;
  }

  // return the cached schedule
  return scheduleCache.cachedSchedules[scheduleId].schedule;
}

function useCachedCourses(scheduleId: string): CourseMap | null {
  const scheduleCache = useContext(CachedScheduleContext);
  const [
    coursesForSchedule,
    setCoursesForSchedule,
  ] = useState<CourseMap | null>(null);

  useEffect(() => {
    if (
      !scheduleCache ||
      !scheduleCache.cachedSchedules.hasOwnProperty(scheduleId)
    ) {
      return;
    }

    scheduleCache.cachedSchedules[scheduleId].courses.then((courseMap) => {
      setCoursesForSchedule(courseMap);
    });
  }, [scheduleCache, scheduleId]);

  return coursesForSchedule;
}

function useAllCachedSchedules(): Array<Schedule> | null {
  const scheduleCache = useContext(CachedScheduleContext);

  if (!scheduleCache || scheduleCache.status !== ScheduleCacheStatus.LOADED) {
    return null;
  }

  return Object.keys(scheduleCache.cachedSchedules).map(
    (key) => scheduleCache.cachedSchedules[key].schedule
  );
}

export {
  ProvideCachedSchedules,
  useCachedSchedule,
  useCachedCourses,
  useAllCachedSchedules,
};
