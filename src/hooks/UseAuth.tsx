import { Auth, Hub } from "aws-amplify";
import React, { createContext, useContext, useEffect, useState } from "react";

// Hook for managing auth state by wrapping the AWS amplify auth API
// see (https://usehooks.com/useAuth/) for info on auth hooks/contexts created in this file.
// TODO: use better typings for this stuff - start here (https://gist.github.com/groundedSAGE/995dc2e14845980fdc547c8ba510169c)?
// TODO: could we just get rid of this function and put its contents in ProvideAuth?
function useProvideAuth() {
  const [user, setUser] = useState(null);
  const [authPending, setAuthPending] = useState(true);

  /**
   * Get authentication state and subscribe to future changes when component mounts.
   *
   * Because this sets state in the callback it will cause any component that
   * utilizes this hook to re-render with the latest auth object.
   */
  useEffect(() => {
    // get the initial "signedIn" state
    Auth.currentAuthenticatedUser()
      .then((user) => {
        setUser(user);
        setAuthPending(false);
      })
      .catch((error) => {
        setUser(null);
        setAuthPending(false);
      });

    // update the "signedIn" state when the user signs in or out
    Hub.listen("auth", (data) => {
      switch (data.payload.event) {
        case "signIn":
          setUser(data.payload.data);
          break;
        case "signOut":
          setUser(null);
          break;
      }
      setAuthPending(false);
    });
  }, []);

  // TODO: get rid of this eventually
  useEffect(() => {
    //console.log("User state changed: ", user)
  }, [user]);

  // Return the user object and auth methods
  // TODO: migrate direct amplify auth calls to use these wrapped methods.
  return {
    user,
    authPending,
    signin: (email: string, password: string) => {
      console.log("signing in");
    },
    signup: (email: string, password: string) => {
      console.log("signing up");
    },
    signout: () => {
      Auth.signOut()
        .then(() => {
          setUser(null);
        })
        .catch((error) => {
          console.log("Error while signing out:", error);
        });
    },
    sendPasswordResetEmail: (email: string) => {
      console.log("sending password reset email");
    },
    confirmPasswordReset: (code: string, password: string) => {
      console.log("confirming password reset");
    },
  };
}

// Authentication context to make available to child components of ProvideAuth
// (see: https://reactjs.org/docs/context.html) for more info on react contexts.
const AuthContext = createContext(null as any | null);

/**
 * Provider component that wraps the app and makes auth object available to any
 * child component that calls useAuth().
 */
function ProvideAuth(props: { children: any }) {
  const auth = useProvideAuth();
  return (
    <AuthContext.Provider value={auth}>{props.children}</AuthContext.Provider>
  );
}

/**
 * Hook for child components to get the auth object and re-render when it changes.
 */
function useAuth() {
  return useContext(AuthContext);
}

export { ProvideAuth, useAuth };
