import { Schedule } from "API";
import CourseMap from "./CourseMap";

/**
 * Models an object used to track and modify the state of a user's schedules
 */
interface ManagedSchedule {
  // information about the schedule
  schedule: Schedule;

  // information about courses in the schedule (fetched asynchronously)
  courses: Promise<CourseMap>;
}

export default ManagedSchedule;
