import { Course } from "API";

interface CourseMap {
  [courseId: string]: Course;
}

export default CourseMap;
