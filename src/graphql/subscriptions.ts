/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateCourse = /* GraphQL */ `
  subscription OnCreateCourse {
    onCreateCourse {
      id
      course_title
      subject
      course_code
      term
      levels
      sections {
        crn
        description
        section_type
        section_number
        reg_start
        reg_end
        attributes
        campus
        schedule_type_alt
        method
        credits
        meeting_times {
          type
          location
          schedule_type
          start_time
          end_time
          duration
          rrule
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCourse = /* GraphQL */ `
  subscription OnUpdateCourse {
    onUpdateCourse {
      id
      course_title
      subject
      course_code
      term
      levels
      sections {
        crn
        description
        section_type
        section_number
        reg_start
        reg_end
        attributes
        campus
        schedule_type_alt
        method
        credits
        meeting_times {
          type
          location
          schedule_type
          start_time
          end_time
          duration
          rrule
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCourse = /* GraphQL */ `
  subscription OnDeleteCourse {
    onDeleteCourse {
      id
      course_title
      subject
      course_code
      term
      levels
      sections {
        crn
        description
        section_type
        section_number
        reg_start
        reg_end
        attributes
        campus
        schedule_type_alt
        method
        credits
        meeting_times {
          type
          location
          schedule_type
          start_time
          end_time
          duration
          rrule
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSchedule = /* GraphQL */ `
  subscription OnCreateSchedule {
    onCreateSchedule {
      id
      owner
      name
      term
      items {
        course_id
        selected_crns
        color
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSchedule = /* GraphQL */ `
  subscription OnUpdateSchedule {
    onUpdateSchedule {
      id
      owner
      name
      term
      items {
        course_id
        selected_crns
        color
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSchedule = /* GraphQL */ `
  subscription OnDeleteSchedule {
    onDeleteSchedule {
      id
      owner
      name
      term
      items {
        course_id
        selected_crns
        color
      }
      createdAt
      updatedAt
    }
  }
`;
