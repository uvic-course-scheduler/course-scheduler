/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getCourse = /* GraphQL */ `
  query GetCourse($id: ID!) {
    getCourse(id: $id) {
      id
      course_title
      subject
      course_code
      term
      levels
      sections {
        crn
        description
        section_type
        section_number
        reg_start
        reg_end
        attributes
        campus
        schedule_type_alt
        method
        credits
        meeting_times {
          type
          location
          schedule_type
          start_time
          end_time
          duration
          rrule
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const listCourses = /* GraphQL */ `
  query ListCourses(
    $id: ID
    $filter: ModelCourseFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listCourses(
      id: $id
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        id
        course_title
        subject
        course_code
        term
        levels
        sections {
          crn
          description
          section_type
          section_number
          reg_start
          reg_end
          attributes
          campus
          schedule_type_alt
          method
          credits
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const coursesByTerm = /* GraphQL */ `
  query CoursesByTerm(
    $term: String
    $sortDirection: ModelSortDirection
    $filter: ModelCourseFilterInput
    $limit: Int
    $nextToken: String
  ) {
    coursesByTerm(
      term: $term
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        course_title
        subject
        course_code
        term
        levels
        sections {
          crn
          description
          section_type
          section_number
          reg_start
          reg_end
          attributes
          campus
          schedule_type_alt
          method
          credits
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const coursesByCode = /* GraphQL */ `
  query CoursesByCode(
    $term: String
    $subjectCourse_code: ModelCourseCoursesByCodeCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCourseFilterInput
    $limit: Int
    $nextToken: String
  ) {
    coursesByCode(
      term: $term
      subjectCourse_code: $subjectCourse_code
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        course_title
        subject
        course_code
        term
        levels
        sections {
          crn
          description
          section_type
          section_number
          reg_start
          reg_end
          attributes
          campus
          schedule_type_alt
          method
          credits
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSchedule = /* GraphQL */ `
  query GetSchedule($id: ID!) {
    getSchedule(id: $id) {
      id
      owner
      name
      term
      items {
        course_id
        selected_crns
        color
      }
      createdAt
      updatedAt
    }
  }
`;
export const listSchedules = /* GraphQL */ `
  query ListSchedules(
    $id: ID
    $filter: ModelScheduleFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listSchedules(
      id: $id
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        id
        owner
        name
        term
        items {
          course_id
          selected_crns
          color
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const schedulesByOwner = /* GraphQL */ `
  query SchedulesByOwner(
    $owner: String
    $sortDirection: ModelSortDirection
    $filter: ModelScheduleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    schedulesByOwner(
      owner: $owner
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        owner
        name
        term
        items {
          course_id
          selected_crns
          color
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
