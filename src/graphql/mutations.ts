/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createCourse = /* GraphQL */ `
  mutation CreateCourse(
    $input: CreateCourseInput!
    $condition: ModelCourseConditionInput
  ) {
    createCourse(input: $input, condition: $condition) {
      id
      course_title
      subject
      course_code
      term
      levels
      sections {
        crn
        description
        section_type
        section_number
        reg_start
        reg_end
        attributes
        campus
        schedule_type_alt
        method
        credits
        meeting_times {
          type
          location
          schedule_type
          start_time
          end_time
          duration
          rrule
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateCourse = /* GraphQL */ `
  mutation UpdateCourse(
    $input: UpdateCourseInput!
    $condition: ModelCourseConditionInput
  ) {
    updateCourse(input: $input, condition: $condition) {
      id
      course_title
      subject
      course_code
      term
      levels
      sections {
        crn
        description
        section_type
        section_number
        reg_start
        reg_end
        attributes
        campus
        schedule_type_alt
        method
        credits
        meeting_times {
          type
          location
          schedule_type
          start_time
          end_time
          duration
          rrule
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteCourse = /* GraphQL */ `
  mutation DeleteCourse(
    $input: DeleteCourseInput!
    $condition: ModelCourseConditionInput
  ) {
    deleteCourse(input: $input, condition: $condition) {
      id
      course_title
      subject
      course_code
      term
      levels
      sections {
        crn
        description
        section_type
        section_number
        reg_start
        reg_end
        attributes
        campus
        schedule_type_alt
        method
        credits
        meeting_times {
          type
          location
          schedule_type
          start_time
          end_time
          duration
          rrule
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const createSchedule = /* GraphQL */ `
  mutation CreateSchedule(
    $input: CreateScheduleInput!
    $condition: ModelScheduleConditionInput
  ) {
    createSchedule(input: $input, condition: $condition) {
      id
      owner
      name
      term
      items {
        course_id
        selected_crns
        color
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateSchedule = /* GraphQL */ `
  mutation UpdateSchedule(
    $input: UpdateScheduleInput!
    $condition: ModelScheduleConditionInput
  ) {
    updateSchedule(input: $input, condition: $condition) {
      id
      owner
      name
      term
      items {
        course_id
        selected_crns
        color
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteSchedule = /* GraphQL */ `
  mutation DeleteSchedule(
    $input: DeleteScheduleInput!
    $condition: ModelScheduleConditionInput
  ) {
    deleteSchedule(input: $input, condition: $condition) {
      id
      owner
      name
      term
      items {
        course_id
        selected_crns
        color
      }
      createdAt
      updatedAt
    }
  }
`;
