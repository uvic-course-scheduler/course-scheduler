/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateCourseInput = {
  id?: string | null;
  course_title?: string | null;
  subject: string;
  course_code: string;
  term: string;
  levels?: string | null;
  sections?: Array<SectionsInput | null> | null;
};

export type SectionsInput = {
  crn?: string | null;
  description?: string | null;
  section_type?: string | null;
  section_number?: string | null;
  reg_start?: string | null;
  reg_end?: string | null;
  attributes?: string | null;
  campus?: string | null;
  schedule_type_alt?: string | null;
  method?: string | null;
  credits?: string | null;
  meeting_times?: Array<MeetingTimesInput | null> | null;
};

export type MeetingTimesInput = {
  type?: string | null;
  location?: string | null;
  schedule_type?: string | null;
  start_time?: string | null;
  end_time?: string | null;
  duration?: string | null;
  rrule?: string | null;
  instructors?: Array<InstructorsInput | null> | null;
};

export type InstructorsInput = {
  name?: string | null;
  email?: string | null;
};

export type ModelCourseConditionInput = {
  course_title?: ModelStringInput | null;
  subject?: ModelStringInput | null;
  course_code?: ModelStringInput | null;
  term?: ModelStringInput | null;
  levels?: ModelStringInput | null;
  and?: Array<ModelCourseConditionInput | null> | null;
  or?: Array<ModelCourseConditionInput | null> | null;
  not?: ModelCourseConditionInput | null;
};

export type ModelStringInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}

export type ModelSizeInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
};

export type Course = {
  __typename: "Course";
  id?: string;
  course_title?: string | null;
  subject?: string;
  course_code?: string;
  term?: string;
  levels?: string | null;
  sections?: Array<Sections | null> | null;
  createdAt?: string;
  updatedAt?: string;
};

export type Sections = {
  __typename: "Sections";
  crn?: string | null;
  description?: string | null;
  section_type?: string | null;
  section_number?: string | null;
  reg_start?: string | null;
  reg_end?: string | null;
  attributes?: string | null;
  campus?: string | null;
  schedule_type_alt?: string | null;
  method?: string | null;
  credits?: string | null;
  meeting_times?: Array<MeetingTimes | null> | null;
};

export type MeetingTimes = {
  __typename: "MeetingTimes";
  type?: string | null;
  location?: string | null;
  schedule_type?: string | null;
  start_time?: string | null;
  end_time?: string | null;
  duration?: string | null;
  rrule?: string | null;
  instructors?: Array<Instructors | null> | null;
};

export type Instructors = {
  __typename: "Instructors";
  name?: string | null;
  email?: string | null;
};

export type UpdateCourseInput = {
  id: string;
  course_title?: string | null;
  subject?: string | null;
  course_code?: string | null;
  term?: string | null;
  levels?: string | null;
  sections?: Array<SectionsInput | null> | null;
};

export type DeleteCourseInput = {
  id: string;
};

export type CreateScheduleInput = {
  id?: string | null;
  owner?: string | null;
  name: string;
  term: string;
  items: Array<ScheduleItemInput>;
};

export type ScheduleItemInput = {
  course_id: string;
  selected_crns: Array<string>;
  color?: string | null;
};

export type ModelScheduleConditionInput = {
  name?: ModelStringInput | null;
  term?: ModelStringInput | null;
  and?: Array<ModelScheduleConditionInput | null> | null;
  or?: Array<ModelScheduleConditionInput | null> | null;
  not?: ModelScheduleConditionInput | null;
};

export type Schedule = {
  __typename: "Schedule";
  id?: string;
  owner?: string | null;
  name?: string;
  term?: string;
  items?: Array<ScheduleItem>;
  createdAt?: string;
  updatedAt?: string;
};

export type ScheduleItem = {
  __typename: "ScheduleItem";
  course_id?: string;
  selected_crns?: Array<string>;
  color?: string | null;
};

export type UpdateScheduleInput = {
  id: string;
  owner?: string | null;
  name?: string | null;
  term?: string | null;
  items?: Array<ScheduleItemInput> | null;
};

export type DeleteScheduleInput = {
  id: string;
};

export type ModelCourseFilterInput = {
  id?: ModelIDInput | null;
  course_title?: ModelStringInput | null;
  subject?: ModelStringInput | null;
  course_code?: ModelStringInput | null;
  term?: ModelStringInput | null;
  levels?: ModelStringInput | null;
  and?: Array<ModelCourseFilterInput | null> | null;
  or?: Array<ModelCourseFilterInput | null> | null;
  not?: ModelCourseFilterInput | null;
};

export type ModelIDInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}

export type ModelCourseConnection = {
  __typename: "ModelCourseConnection";
  items?: Array<Course | null> | null;
  nextToken?: string | null;
};

export type ModelCourseCoursesByCodeCompositeKeyConditionInput = {
  eq?: ModelCourseCoursesByCodeCompositeKeyInput | null;
  le?: ModelCourseCoursesByCodeCompositeKeyInput | null;
  lt?: ModelCourseCoursesByCodeCompositeKeyInput | null;
  ge?: ModelCourseCoursesByCodeCompositeKeyInput | null;
  gt?: ModelCourseCoursesByCodeCompositeKeyInput | null;
  between?: Array<ModelCourseCoursesByCodeCompositeKeyInput | null> | null;
  beginsWith?: ModelCourseCoursesByCodeCompositeKeyInput | null;
};

export type ModelCourseCoursesByCodeCompositeKeyInput = {
  subject?: string | null;
  course_code?: string | null;
};

export type ModelScheduleFilterInput = {
  id?: ModelIDInput | null;
  owner?: ModelStringInput | null;
  name?: ModelStringInput | null;
  term?: ModelStringInput | null;
  and?: Array<ModelScheduleFilterInput | null> | null;
  or?: Array<ModelScheduleFilterInput | null> | null;
  not?: ModelScheduleFilterInput | null;
};

export type ModelScheduleConnection = {
  __typename: "ModelScheduleConnection";
  items?: Array<Schedule | null> | null;
  nextToken?: string | null;
};

export type CreateCourseMutationVariables = {
  input?: CreateCourseInput;
  condition?: ModelCourseConditionInput | null;
};

export type CreateCourseMutation = {
  createCourse?: {
    __typename: "Course";
    id: string;
    course_title?: string | null;
    subject: string;
    course_code: string;
    term: string;
    levels?: string | null;
    sections?: Array<{
      __typename: "Sections";
      crn?: string | null;
      description?: string | null;
      section_type?: string | null;
      section_number?: string | null;
      reg_start?: string | null;
      reg_end?: string | null;
      attributes?: string | null;
      campus?: string | null;
      schedule_type_alt?: string | null;
      method?: string | null;
      credits?: string | null;
      meeting_times?: Array<{
        __typename: "MeetingTimes";
        type?: string | null;
        location?: string | null;
        schedule_type?: string | null;
        start_time?: string | null;
        end_time?: string | null;
        duration?: string | null;
        rrule?: string | null;
      } | null> | null;
    } | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type UpdateCourseMutationVariables = {
  input?: UpdateCourseInput;
  condition?: ModelCourseConditionInput | null;
};

export type UpdateCourseMutation = {
  updateCourse?: {
    __typename: "Course";
    id: string;
    course_title?: string | null;
    subject: string;
    course_code: string;
    term: string;
    levels?: string | null;
    sections?: Array<{
      __typename: "Sections";
      crn?: string | null;
      description?: string | null;
      section_type?: string | null;
      section_number?: string | null;
      reg_start?: string | null;
      reg_end?: string | null;
      attributes?: string | null;
      campus?: string | null;
      schedule_type_alt?: string | null;
      method?: string | null;
      credits?: string | null;
      meeting_times?: Array<{
        __typename: "MeetingTimes";
        type?: string | null;
        location?: string | null;
        schedule_type?: string | null;
        start_time?: string | null;
        end_time?: string | null;
        duration?: string | null;
        rrule?: string | null;
      } | null> | null;
    } | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type DeleteCourseMutationVariables = {
  input?: DeleteCourseInput;
  condition?: ModelCourseConditionInput | null;
};

export type DeleteCourseMutation = {
  deleteCourse?: {
    __typename: "Course";
    id: string;
    course_title?: string | null;
    subject: string;
    course_code: string;
    term: string;
    levels?: string | null;
    sections?: Array<{
      __typename: "Sections";
      crn?: string | null;
      description?: string | null;
      section_type?: string | null;
      section_number?: string | null;
      reg_start?: string | null;
      reg_end?: string | null;
      attributes?: string | null;
      campus?: string | null;
      schedule_type_alt?: string | null;
      method?: string | null;
      credits?: string | null;
      meeting_times?: Array<{
        __typename: "MeetingTimes";
        type?: string | null;
        location?: string | null;
        schedule_type?: string | null;
        start_time?: string | null;
        end_time?: string | null;
        duration?: string | null;
        rrule?: string | null;
      } | null> | null;
    } | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type CreateScheduleMutationVariables = {
  input?: CreateScheduleInput;
  condition?: ModelScheduleConditionInput | null;
};

export type CreateScheduleMutation = {
  createSchedule?: {
    __typename: "Schedule";
    id: string;
    owner?: string | null;
    name: string;
    term: string;
    items: Array<{
      __typename: "ScheduleItem";
      course_id: string;
      selected_crns: Array<string>;
      color?: string | null;
    }>;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type UpdateScheduleMutationVariables = {
  input?: UpdateScheduleInput;
  condition?: ModelScheduleConditionInput | null;
};

export type UpdateScheduleMutation = {
  updateSchedule?: {
    __typename: "Schedule";
    id: string;
    owner?: string | null;
    name: string;
    term: string;
    items: Array<{
      __typename: "ScheduleItem";
      course_id: string;
      selected_crns: Array<string>;
      color?: string | null;
    }>;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type DeleteScheduleMutationVariables = {
  input?: DeleteScheduleInput;
  condition?: ModelScheduleConditionInput | null;
};

export type DeleteScheduleMutation = {
  deleteSchedule?: {
    __typename: "Schedule";
    id: string;
    owner?: string | null;
    name: string;
    term: string;
    items: Array<{
      __typename: "ScheduleItem";
      course_id: string;
      selected_crns: Array<string>;
      color?: string | null;
    }>;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type GetCourseQueryVariables = {
  id?: string;
};

export type GetCourseQuery = {
  getCourse?: {
    __typename: "Course";
    id: string;
    course_title?: string | null;
    subject: string;
    course_code: string;
    term: string;
    levels?: string | null;
    sections?: Array<{
      __typename: "Sections";
      crn?: string | null;
      description?: string | null;
      section_type?: string | null;
      section_number?: string | null;
      reg_start?: string | null;
      reg_end?: string | null;
      attributes?: string | null;
      campus?: string | null;
      schedule_type_alt?: string | null;
      method?: string | null;
      credits?: string | null;
      meeting_times?: Array<{
        __typename: "MeetingTimes";
        type?: string | null;
        location?: string | null;
        schedule_type?: string | null;
        start_time?: string | null;
        end_time?: string | null;
        duration?: string | null;
        rrule?: string | null;
      } | null> | null;
    } | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type ListCoursesQueryVariables = {
  id?: string | null;
  filter?: ModelCourseFilterInput | null;
  limit?: number | null;
  nextToken?: string | null;
  sortDirection?: ModelSortDirection | null;
};

export type ListCoursesQuery = {
  listCourses?: {
    __typename: "ModelCourseConnection";
    items?: Array<{
      __typename: "Course";
      id: string;
      course_title?: string | null;
      subject: string;
      course_code: string;
      term: string;
      levels?: string | null;
      sections?: Array<{
        __typename: "Sections";
        crn?: string | null;
        description?: string | null;
        section_type?: string | null;
        section_number?: string | null;
        reg_start?: string | null;
        reg_end?: string | null;
        attributes?: string | null;
        campus?: string | null;
        schedule_type_alt?: string | null;
        method?: string | null;
        credits?: string | null;
      } | null> | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
};

export type CoursesByTermQueryVariables = {
  term?: string | null;
  sortDirection?: ModelSortDirection | null;
  filter?: ModelCourseFilterInput | null;
  limit?: number | null;
  nextToken?: string | null;
};

export type CoursesByTermQuery = {
  coursesByTerm?: {
    __typename: "ModelCourseConnection";
    items?: Array<{
      __typename: "Course";
      id: string;
      course_title?: string | null;
      subject: string;
      course_code: string;
      term: string;
      levels?: string | null;
      sections?: Array<{
        __typename: "Sections";
        crn?: string | null;
        description?: string | null;
        section_type?: string | null;
        section_number?: string | null;
        reg_start?: string | null;
        reg_end?: string | null;
        attributes?: string | null;
        campus?: string | null;
        schedule_type_alt?: string | null;
        method?: string | null;
        credits?: string | null;
      } | null> | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
};

export type CoursesByCodeQueryVariables = {
  term?: string | null;
  subjectCourse_code?: ModelCourseCoursesByCodeCompositeKeyConditionInput | null;
  sortDirection?: ModelSortDirection | null;
  filter?: ModelCourseFilterInput | null;
  limit?: number | null;
  nextToken?: string | null;
};

export type CoursesByCodeQuery = {
  coursesByCode?: {
    __typename: "ModelCourseConnection";
    items?: Array<{
      __typename: "Course";
      id: string;
      course_title?: string | null;
      subject: string;
      course_code: string;
      term: string;
      levels?: string | null;
      sections?: Array<{
        __typename: "Sections";
        crn?: string | null;
        description?: string | null;
        section_type?: string | null;
        section_number?: string | null;
        reg_start?: string | null;
        reg_end?: string | null;
        attributes?: string | null;
        campus?: string | null;
        schedule_type_alt?: string | null;
        method?: string | null;
        credits?: string | null;
      } | null> | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
};

export type GetScheduleQueryVariables = {
  id?: string;
};

export type GetScheduleQuery = {
  getSchedule?: {
    __typename: "Schedule";
    id: string;
    owner?: string | null;
    name: string;
    term: string;
    items: Array<{
      __typename: "ScheduleItem";
      course_id: string;
      selected_crns: Array<string>;
      color?: string | null;
    }>;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type ListSchedulesQueryVariables = {
  id?: string | null;
  filter?: ModelScheduleFilterInput | null;
  limit?: number | null;
  nextToken?: string | null;
  sortDirection?: ModelSortDirection | null;
};

export type ListSchedulesQuery = {
  listSchedules?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      owner?: string | null;
      name: string;
      term: string;
      items: Array<{
        __typename: "ScheduleItem";
        course_id: string;
        selected_crns: Array<string>;
        color?: string | null;
      }>;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
};

export type SchedulesByOwnerQueryVariables = {
  owner?: string | null;
  sortDirection?: ModelSortDirection | null;
  filter?: ModelScheduleFilterInput | null;
  limit?: number | null;
  nextToken?: string | null;
};

export type SchedulesByOwnerQuery = {
  schedulesByOwner?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      owner?: string | null;
      name: string;
      term: string;
      items: Array<{
        __typename: "ScheduleItem";
        course_id: string;
        selected_crns: Array<string>;
        color?: string | null;
      }>;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
};

export type OnCreateCourseSubscription = {
  onCreateCourse?: {
    __typename: "Course";
    id: string;
    course_title?: string | null;
    subject: string;
    course_code: string;
    term: string;
    levels?: string | null;
    sections?: Array<{
      __typename: "Sections";
      crn?: string | null;
      description?: string | null;
      section_type?: string | null;
      section_number?: string | null;
      reg_start?: string | null;
      reg_end?: string | null;
      attributes?: string | null;
      campus?: string | null;
      schedule_type_alt?: string | null;
      method?: string | null;
      credits?: string | null;
      meeting_times?: Array<{
        __typename: "MeetingTimes";
        type?: string | null;
        location?: string | null;
        schedule_type?: string | null;
        start_time?: string | null;
        end_time?: string | null;
        duration?: string | null;
        rrule?: string | null;
      } | null> | null;
    } | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type OnUpdateCourseSubscription = {
  onUpdateCourse?: {
    __typename: "Course";
    id: string;
    course_title?: string | null;
    subject: string;
    course_code: string;
    term: string;
    levels?: string | null;
    sections?: Array<{
      __typename: "Sections";
      crn?: string | null;
      description?: string | null;
      section_type?: string | null;
      section_number?: string | null;
      reg_start?: string | null;
      reg_end?: string | null;
      attributes?: string | null;
      campus?: string | null;
      schedule_type_alt?: string | null;
      method?: string | null;
      credits?: string | null;
      meeting_times?: Array<{
        __typename: "MeetingTimes";
        type?: string | null;
        location?: string | null;
        schedule_type?: string | null;
        start_time?: string | null;
        end_time?: string | null;
        duration?: string | null;
        rrule?: string | null;
      } | null> | null;
    } | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type OnDeleteCourseSubscription = {
  onDeleteCourse?: {
    __typename: "Course";
    id: string;
    course_title?: string | null;
    subject: string;
    course_code: string;
    term: string;
    levels?: string | null;
    sections?: Array<{
      __typename: "Sections";
      crn?: string | null;
      description?: string | null;
      section_type?: string | null;
      section_number?: string | null;
      reg_start?: string | null;
      reg_end?: string | null;
      attributes?: string | null;
      campus?: string | null;
      schedule_type_alt?: string | null;
      method?: string | null;
      credits?: string | null;
      meeting_times?: Array<{
        __typename: "MeetingTimes";
        type?: string | null;
        location?: string | null;
        schedule_type?: string | null;
        start_time?: string | null;
        end_time?: string | null;
        duration?: string | null;
        rrule?: string | null;
      } | null> | null;
    } | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type OnCreateScheduleSubscription = {
  onCreateSchedule?: {
    __typename: "Schedule";
    id: string;
    owner?: string | null;
    name: string;
    term: string;
    items: Array<{
      __typename: "ScheduleItem";
      course_id: string;
      selected_crns: Array<string>;
      color?: string | null;
    }>;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type OnUpdateScheduleSubscription = {
  onUpdateSchedule?: {
    __typename: "Schedule";
    id: string;
    owner?: string | null;
    name: string;
    term: string;
    items: Array<{
      __typename: "ScheduleItem";
      course_id: string;
      selected_crns: Array<string>;
      color?: string | null;
    }>;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type OnDeleteScheduleSubscription = {
  onDeleteSchedule?: {
    __typename: "Schedule";
    id: string;
    owner?: string | null;
    name: string;
    term: string;
    items: Array<{
      __typename: "ScheduleItem";
      course_id: string;
      selected_crns: Array<string>;
      color?: string | null;
    }>;
    createdAt: string;
    updatedAt: string;
  } | null;
};
