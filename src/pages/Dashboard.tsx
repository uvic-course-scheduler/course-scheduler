import { CircularProgress } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import DashboardEditItem from "components/dashboard/DashboardEditItem";
import DashboardScheduleItem from "components/dashboard/DashboardScheduleItem";
import { Header } from "components/header/Header";
import { useAllCachedSchedules } from "hooks/UseCachedSchedule";
import React, { useState } from "react";

const MAX_SCHEDULES = 5;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
    },
    addSchedule: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    title: {
      textAlign: "center",
    },
  })
);

function Dashboard() {
  const classes = useStyles();
  const [editingNewSchedule, setEditingNewSchedule] = useState(false);
  const schedules = useAllCachedSchedules();

  function handleAddBtnClick() {
    setEditingNewSchedule(true);
  }

  function handleScheduleEditComplete() {
    setEditingNewSchedule(false);
  }

  return (
    <Header hideBackButton>
      <Typography className={classes.title} variant="h4">
        Your Schedules
      </Typography>
      <Grid container spacing={2} justify="center" alignItems="center">
        {!schedules ? (
          <Box mt={10} textAlign="center">
            <CircularProgress disableShrink />
          </Box>
        ) : (
          <Grid item xs={12} sm={8} xl={6}>
            {schedules.map(
              (schedule) =>
                schedule.id && (
                  <Box mt={1} mb={1} key={schedule.id}>
                    <DashboardScheduleItem scheduleId={schedule.id} />
                  </Box>
                )
            )}
            {editingNewSchedule ? (
              <DashboardEditItem onEditComplete={handleScheduleEditComplete} />
            ) : (
              <Box mt={3} className={classes.addSchedule}>
                <Fab
                  color="primary"
                  aria-label="add schedule"
                  onClick={handleAddBtnClick}
                  disabled={!schedules || schedules.length > MAX_SCHEDULES}
                >
                  <AddIcon />
                </Fab>
              </Box>
            )}
          </Grid>
        )}
      </Grid>
    </Header>
  );
}

export default Dashboard;
