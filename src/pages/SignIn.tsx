import { Snackbar } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { Alert } from "@material-ui/lab";
import { Auth } from "aws-amplify";
import SignInForm from "components/auth/SignInForm";
import { Header } from "components/header/Header";
import React, { useState } from "react";
import { useHistory, useLocation } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  }
}));

function SignIn() {
  const classes = useStyles();
  
  // disable the signin button while true
  const [signInLoading, setSignInLoading] = useState(false);

  // signinError
  const [signInError, setSignInError] = useState("");

  // react router hook to change the current route (used to forward to dashboard)
  const history = useHistory();
  const location = useLocation();

  // TODO: icky "as any"
  const locationState = location.state as any
  const locationFrom = locationState?.from ? locationState.from : null;

  console.log("at signin coming from", locationFrom)

  /**
   * Executed when the "Sign In" button is pressed.
   */
  async function handleSignIn(email: string, password: string) {
    setSignInLoading(true);

    // try to sign in
    try {
      await Auth.signIn(email, password);
    } catch (error) {
      console.log("error signing in:", error);
      setSignInError(error.message)
      setSignInLoading(false);
      return;
    }

    // if the user was redirected to signin by a PrivateRoute, they should be returned to the original page
    if (locationFrom) {
      history.replace(locationFrom);
    } else {
      history.push("/dashboard");
    }
  }

  return (
    <Header>
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          
          {
            locationFrom ? 
            <Typography component="h1" variant="h5">
              Sign in to view this page
            </Typography> :
            <Typography component="h1" variant="h5">
              Sign in to start scheduling!
            </Typography>
          }
          
          <SignInForm 
            onSubmit={handleSignIn} 
            submitError={!!signInError}
            submitDisabled={signInLoading}
          />
        </div>
      </Container>
      <Snackbar open={!!signInError} autoHideDuration={6000}>
        <Alert variant="filled" severity="error">
          {signInError}
        </Alert>
      </Snackbar>
    </Header>
  );
}

export default SignIn;
