import { Container, Grid, makeStyles, Typography } from "@material-ui/core";
import { CrnTable } from "components/CrnTable";
import { Header } from "components/header/Header";
import { useCachedCourses, useCachedSchedule } from "hooks/UseCachedSchedule";
import React from "react";
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  center: {
    textAlign: "center",
  },
  table: {
    maxWidth: 650,
  },
}));

function Register() {
  const classes = useStyles();

  // get the id of the schedule to display from the url
  let { id } = useParams<{ id: string }>();

  // get schedule and course data
  const schedule = useCachedSchedule(id);
  const courses = useCachedCourses(id);

  return (
    <Header title="Register">
      <Container className={classes.center}>
        <Typography variant="h4">Register</Typography>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="flex-start"
          spacing={1}
        >
          <Grid item xs={12} md={8}>
            {schedule && courses ? (
              <CrnTable schedule={schedule} courses={courses} />
            ) : (
              <p>Sorry, there we weren't able to load your schedule.</p>
            )}
          </Grid>
        </Grid>
      </Container>
    </Header>
  );
}

export default Register;
