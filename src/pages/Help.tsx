import { Container } from "@material-ui/core";
import { Header } from "components/header/Header";
import React from "react";

function Help() {
  return (
    <Header>
      <Container>
        <h1>Deciding what courses to take</h1>
        Before you can use this tool, you need to know what courses you want to
        schedule.&nbsp;
        <a
          href="https://www.uvic.ca/students/undergraduate/program-planning/program-worksheets/index.php"
          rel="noopener noreferrer"
          target="_blank"
        >
          UVic's program planning worksheets
        </a>
        &nbsp;are a good place to start.
      </Container>
    </Header>
  );
}

export default Help;
