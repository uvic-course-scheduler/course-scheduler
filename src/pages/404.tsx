import { Box, Grid, Typography } from "@material-ui/core";
import { Header } from "components/header/Header";
import React from "react";
import Martlet from "../images/chunky_martlet-cropped.svg";

function PageNotFound() {
  return (
    <Header>
      <Box textAlign="center">
        <Typography variant="h3">Page Not Found</Typography>
      </Box>
      <Grid container justify="center">
        <Grid container item xs={6} md={4}>
          <img src={Martlet} alt="Large UVic Martlet" />
        </Grid>
      </Grid>
      <Box textAlign="center">
        <Typography variant="h5">Sorry about that.</Typography>
      </Box>
    </Header>
  );
}

export default PageNotFound;
