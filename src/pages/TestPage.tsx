import API, { GraphQLResult, GRAPHQL_AUTH_MODE } from '@aws-amplify/api';
import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";
import "@fullcalendar/timegrid/main.css";
import { GetScheduleQuery } from 'API';
import { Auth } from 'aws-amplify';
import AWSAppSyncClient, { AUTH_TYPE } from 'aws-appsync';
import React, { useEffect } from 'react';
import awsconfig from '../aws-exports';
import * as queries from '../graphql/queries';

function TestPage() {
  const scheduleId = "c37d2081-b009-40a9-89cb-1fa581e98813"

  useEffect(() => {

    const client = new AWSAppSyncClient({
      url: awsconfig.aws_appsync_graphqlEndpoint,
      region: awsconfig.aws_appsync_region,
      auth: {
        type: AUTH_TYPE.AWS_IAM,
        credentials: () => Auth.currentCredentials(),
      },
    });
    console.log(client);

    // get initial schedule (otherwise schedule would not show up until subscription update)
    // TODO: We are using API_KEY here so you don't have to be logged in to view a 
    // schedule (for sharing), but AWS_IAM might be a better option long-term.
    (API.graphql({
      query: queries.listSchedules,
      variables: {},
      authMode: GRAPHQL_AUTH_MODE.API_KEY
    }) as Promise<GraphQLResult<object>>).then(
      (result: GraphQLResult<GetScheduleQuery>) => {
        const schedule = result.data?.getSchedule;
        console.log(result);
      }, (error) => {
        console.log("Schedule get failed. :(", error)
      });
  }, []);

  return (
    <>
      test page
    </>
  );
}

export default TestPage;
