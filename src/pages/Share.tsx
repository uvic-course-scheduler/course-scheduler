import { Grid } from "@material-ui/core";
import CourseCalendar from "components/CourseCalendar";
import { Header } from "components/header/Header";
import { useCourses, useSchedule } from "hooks/UseSchedule";
import React from "react";
import { useParams } from "react-router-dom";

// TODO: Share page should display schedule title
function Share() {
  // get the id of the schedule to display from the url
  let { id } = useParams<{ id: string }>();

  return (
    <Header>
      <Grid container justify="center">
        <Grid item xs={12} md={8}>
          <CourseCalendar schedule={useSchedule(id)} courses={useCourses(id)} />
        </Grid>
      </Grid>
    </Header>
  );
}

export default Share;
