import { Header } from "components/header/Header";
import React from "react";

function Settings() {
  return (
    <Header>
      <div>Settings</div>
    </Header>
  );
}

export default Settings;
