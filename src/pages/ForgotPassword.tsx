import { Box } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { Auth } from 'aws-amplify';
import { Header } from "components/header/Header";
import React, { useState } from 'react';
import { Link as RouterLink, useHistory } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function ForgotPassword() {
  return (
    <Header>
      <ForgotPasswordForm />
    </Header>
  );
}

function ForgotPasswordForm() {
  const classes = useStyles();
  const [formState, updateFormState] = useState({
    email: '',
    password: '',
    passwordConfirm: '',
    verificationCode: '',
    awaitingVerify: false
  });
  const [formErrorState] = useState({
    email: false,
    password: false,
    passwordConfirm: false
  });
  const history = useHistory();

  function onFormFieldChange(e: React.ChangeEvent) {
    e.persist();
    const target = e.target as HTMLInputElement;
    updateFormState({ ...formState, [target.name]: target.value })
  }

  async function onResetRequest() {
    try {
      //const test = await Auth.forgotPassword(formState.email);
    } catch (error) {
      console.log('error requesting password reset:', error);
      return
    }

    // transition to the email verification code form
    updateFormState(() => ({ ...formState, awaitingVerify: true }))
  }

  /**
   * Executed when the verify button is pressed.
   */
  async function onVerify() {
    const { email, verificationCode } = formState;

    // check the verification code
    try {
      await Auth.confirmSignUp(email, verificationCode);
    } catch (error) {
      console.log('error verfying email:', error);
    }

    // forward the verified user to the signin page
    history.push("/signin");
  }

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        {
          !formState.awaitingVerify && (
            <>
              <Typography component="h1" variant="h5">
                Request a password reset
              </Typography>
              <Box mt={2}>
                <Typography variant="subtitle2">
                  We'll send a verification code to your email.
                </Typography>
              </Box>
              <TextField
                error={formErrorState.email}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={onFormFieldChange}
              />
              <Button
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={onResetRequest}
              >
                Request Reset
              </Button>
              <Grid container justify="flex-end">
                <Grid item>
                  <Link component={RouterLink} to={"/signin"} variant="body2">
                    Back to sign in
                </Link>
                </Grid>
              </Grid>
            </>
          )
        }
        {
          formState.awaitingVerify && (
            <>
              <Typography component="h1" variant="h5">
                Reset your password
              </Typography>
              <Box mt={2} mb={2}>
                <Typography variant="subtitle2">
                  A verification code has been sent to your email address, please enter it below.
                </Typography>
              </Box>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    label="Verification Code"
                    name="verificationCode"
                    onChange={onFormFieldChange}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={formErrorState.password}
                    variant="outlined"
                    required
                    fullWidth
                    name="password"
                    label="New Password"
                    type="password"
                    autoComplete="new-password"
                    onChange={onFormFieldChange}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={formErrorState.passwordConfirm}
                    variant="outlined"
                    required
                    fullWidth
                    name="passwordConfirm"
                    label="Confirm New Password"
                    type="password"
                    autoComplete="new-password"
                    onChange={onFormFieldChange}
                  />
                </Grid>
              </Grid>
              <Button
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={onVerify}
              >
                Reset Password
              </Button>
            </>
          )
        }
      </div>
    </Container>
  );
}

export default ForgotPassword;
