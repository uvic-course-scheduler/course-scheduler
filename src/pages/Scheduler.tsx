import { Box, makeStyles, Paper } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import CourseCalendar from "components/CourseCalendar";
import ScheduleEditor from "components/editor/ScheduleEditor";
import { Header } from "components/header/Header";
import { useCachedCourses, useCachedSchedule } from "hooks/UseCachedSchedule";
import React from "react";
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  paper: {
    margin: theme.spacing(2, 2),
    display: "flex",
    flexDirection: "column",
  },
  fullHeightDesktop: {
    [theme.breakpoints.up('md')]: {
      height: "calc(100vh - 52px)",
      overflowY: "auto"
    },
  }
}));

function Scheduler() {
  const classes = useStyles();

  // get the id of the schedule to display from the url
  let { id } = useParams<{ id: string }>();

  return (
    <Header title="Scheduler" noMargin>
      <Grid container>
        <Grid item xs={12} md={4} component={Paper} elevation={3} square className={classes.fullHeightDesktop}>
          <div className={classes.paper}>
            <ScheduleEditor scheduleId={id} />
          </div>
        </Grid>
        <Grid item xs={12} md={8} className={classes.fullHeightDesktop}>
          <Box mt={3} ml={3} mr={3}>
            <CourseCalendar
              schedule={useCachedSchedule(id)}
              courses={useCachedCourses(id)}
            />
          </Box>
        </Grid>
      </Grid>
    </Header>
  );
}

export default Scheduler;
