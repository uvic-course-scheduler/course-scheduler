import { Box } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { Auth } from 'aws-amplify';
import { Header } from "components/header/Header";
import React, { useState } from 'react';
import { Link as RouterLink, useHistory } from "react-router-dom";



const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function SignUp() {
  return (
    <Header>
      <SignUpForm />
    </Header>
  );
}

function SignUpForm() {
  const classes = useStyles();
  const [formState, updateFormState] = useState({
    email: '',
    password: '',
    passwordConfirm: '',
    verificationCode: '',
    awaitingVerify: false
  });
  const [formErrorState, updateFormErrorState] = useState({
    email: false,
    password: false,
    passwordConfirm: false
  });
  const history = useHistory();

  function onFormFieldChange(e: React.ChangeEvent) {
    e.persist();
    const target = e.target as HTMLInputElement;
    updateFormState(() => ({ ...formState, [target.name]: target.value }))
  }

  /**
   * Executed when the signup button is pressed.
   */
  async function onSignUp() {
    const { email, password, passwordConfirm } = formState;

    // make sure the fields are populated and that the passwords match
    if (!(email && password) || password !== passwordConfirm) {
      updateFormErrorState({
        ...formErrorState,
        email: !email,
        password: !password,
        passwordConfirm: !passwordConfirm
      })
      return;
    }

    // sign the user up
    try {
      await Auth.signUp({
        username: email,
        password: password,
        attributes: {
          email: email
        }
      });
    } catch (error) {
      console.log('error signing up:', error);
      return
    }

    // transition to the email verification code form
    updateFormState(() => ({ ...formState, awaitingVerify: true }))
  }

  /**
   * Executed when the verify button is pressed.
   */
  async function onVerify() {
    const { email, verificationCode } = formState;

    // check the verification code
    try {
      await Auth.confirmSignUp(email, verificationCode);
    } catch (error) {
      console.log('error verfying email:', error);
    }

    // forward the verified user to the signin page
    history.push("/signin");
  }

  // TODO: separate these the signup and verify forms into their own components
  if (formState.awaitingVerify === false) {
    return (
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Box mb={2}>
            <Typography component="h1" variant="h5">
              Make an account
            </Typography>
          </Box>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                error={formErrorState.email}
                variant="outlined"
                required
                fullWidth
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={onFormFieldChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={formErrorState.password}
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                autoComplete="new-password"
                onChange={onFormFieldChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={formErrorState.passwordConfirm}
                variant="outlined"
                required
                fullWidth
                name="passwordConfirm"
                label="Confirm Password"
                type="password"
                autoComplete="new-password"
                onChange={onFormFieldChange}
              />
            </Grid>
          </Grid>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={onSignUp}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link component={RouterLink} to={"/signin"} variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </div>
      </Container>
    );
  } else {
    return (
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Confirm your email address
          </Typography>
          <Box mt={2} mb={2}>
            <Typography variant="subtitle2">
              A verification code has been sent to your email address, please enter it below.
            </Typography>
          </Box>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                label="Verification Code"
                name="verificationCode"
                onChange={onFormFieldChange}
              />
            </Grid>
          </Grid>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={onVerify}
          >
            Verify
          </Button>
        </div>
      </Container>
    );
  }
}

export default SignUp;
