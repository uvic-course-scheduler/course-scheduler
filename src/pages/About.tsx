import { Container } from "@material-ui/core";
import { Header } from "components/header/Header";
import React from "react";

function About() {
  return (
    <Header>
      <Container>
        <h1>What is schedulecourses.net?</h1>
        This site is inspired by (though not affiliated with) the much-loved and
        now defunct schedulecourses.com. It provides a more streamlined schedule
        building experience than&nbsp;
        <a
          href="https://www.youtube.com/watch?v=2BNqYvRUG-U"
          rel="noopener noreferrer"
          target="_blank"
        >
          that currently offered on uvic.ca
        </a>
        .<h1>Who made this?</h1>
        This site was made by Computer Science and Sofware Engineering students
        from UVic.
      </Container>
    </Header>
  );
}

export default About;
