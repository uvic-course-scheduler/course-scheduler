import { Box, Button, Container, Grid, Typography } from "@material-ui/core";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import { Header } from "components/header/Header";
import YoutubeEmbed from "components/YoutubeEmbed";
import { useAuth } from "hooks/UseAuth";
import React from "react";
import { Link as RouterLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  center: {
    textAlign: "center",
  },
  heading: {
    fontWeight: 700,
    color: theme.palette.primary.main,
    [theme.breakpoints.down("xs")]: {
      fontSize: "9vw",
    },
  },
  subheading: {
    fontWeight: 400,
    marginBottom: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      fontSize: "5vw",
    },
  },
}));

function Landing() {
  const classes = useStyles();
  const auth = useAuth();

  let buttons = null;
  if (!auth.user) {
    buttons = (
      <>
        <Grid item xs={12} md={4}>
          <Button
            fullWidth
            variant="contained"
            disableElevation
            color="primary"
            component={RouterLink}
            to="/signup"
          >
            Sign Up
          </Button>
        </Grid>
        <Grid item xs={12} md={4}>
          <Button
            fullWidth
            variant="outlined"
            color="primary"
            component={RouterLink}
            to="/signin"
          >
            Sign In
          </Button>
        </Grid>
      </>
    );
  } else {
    buttons = (
      <>
        <Grid item xs={12} md={8}>
          <Button
            fullWidth
            variant="outlined"
            color="primary"
            component={RouterLink}
            to="/dashboard"
          >
            Go to your dashboard
          </Button>
        </Grid>
      </>
    );
  }

  return (
    <Header hideBackButton={!auth.user}>
      <Container className={classes.center}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="flex-start"
          spacing={1}
        >
          <Grid item xs={12}>
            <Typography variant="h2" className={classes.heading}>
              schedulecourses.net
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h4" className={classes.subheading}>
              A helpful tool to build your UVic Schedule
            </Typography>
          </Grid>
          {buttons}
          <Grid item xs={12} md={8}>
            <YoutubeEmbed embedId="2BNqYvRUG-U" />
          </Grid>
        </Grid>

        <Box mt={3}>
          Not convinced?&nbsp;
          <Link component={RouterLink} to={"/about"}>
            Learn more.
          </Link>
        </Box>
      </Container>
    </Header>
  );
}

export default Landing;
