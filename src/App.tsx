import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import PrivateRoute from "components/auth/PrivateRoute";
import { ProvideAuth } from "hooks/UseAuth";
import { ProvideCachedSchedules } from "hooks/UseCachedSchedule";
import PageNotFound from "pages/404";
import About from "pages/About";
import Dashboard from "pages/Dashboard";
import ForgotPassword from "pages/ForgotPassword";
import Help from "pages/Help";
import Landing from "pages/Landing";
import Register from "pages/Register";
import Scheduler from "pages/Scheduler";
import Settings from "pages/Settings";
import Share from "pages/Share";
import SignIn from "pages/SignIn";
import SignUp from "pages/SignUp";
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  // (see: https://material-ui.com/customization/palette/)
  const theme = createMuiTheme({
    palette: {
      primary: {
        // light: will be calculated from palette.primary.main,
        main: "#005493",
        // dark: will be calculated from palette.primary.main,
        // contrastText: will be calculated to contrast with palette.primary.main
      },
      secondary: {
        light: "#0066ff",
        main: "#0044ff",
        // dark: will be calculated from palette.secondary.main,
        contrastText: "#ffcc00",
      },
      // Used by `getContrastText()` to maximize the contrast between
      // the background and the text.
      contrastThreshold: 3,
      // Used by the functions below to shift a color's luminance by approximately
      // two indexes within its tonal palette.
      // E.g., shift from Red 500 to Red 300 or Red 700.
      tonalOffset: 0.2,
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <ProvideAuth>
        <Router>
          <Switch>
            <Route exact path="/" component={Landing} />
            <Route exact path="/about" component={About} />
            <Route exact path="/help" component={Help} />

            <Route exact path="/signin" component={SignIn} />
            <Route exact path="/signup" component={SignUp} />
            <Route exact path="/forgotpassword" component={ForgotPassword} />

            <ProvideCachedSchedules>
              <PrivateRoute path="/dashboard">
                <Dashboard />
              </PrivateRoute>
              <PrivateRoute path="/schedule/:id">
                <Scheduler />
              </PrivateRoute>
              <PrivateRoute path="/register/:id">
                <Register />
              </PrivateRoute>
              <PrivateRoute path="/share/:id">
                <Share />
              </PrivateRoute>
              <PrivateRoute path="/settings">
                <Settings />
              </PrivateRoute>
            </ProvideCachedSchedules>
            <Route component={PageNotFound} />
          </Switch>
        </Router>
      </ProvideAuth>
    </ThemeProvider>
  );
}

export default App;
