import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";
import FullCalendar from "@fullcalendar/react";
import rrulePlugin from "@fullcalendar/rrule";
import timeGridPlugin from "@fullcalendar/timegrid";
import "@fullcalendar/timegrid/main.css";
import { Course, MeetingTimes, Schedule, Sections } from "API";
import CourseMap from "hooks/CourseMap";
import React, { useEffect, useState } from "react";
import { rrulestr } from "rrule";
import { getBorderColor, getDefaultColor } from "utils/CourseColorPalette";

interface Event {
  title?: string;
  rrule?: string;
  duration?: {
    seconds: number;
  };
  backgroundColor?: string;
  borderColor?: string;
}

/**
 * Translates a schedule into FullCalendar events.
 * (see: https://fullcalendar.io/docs/event-object)
 *
 * @param schedule The schedule to build events for.
 * @param courseMap Courses data for the courses in the schedule.
 * @returns An array of fullcalendar-compatible events.
 */
function buildFullCalendarEvents(
  schedule: Schedule,
  courseMap: { [key: string]: Course }
): Event[] {
  if (!schedule || !schedule.items || !courseMap) {
    return [];
  }

  return schedule.items
    .flatMap((scheduleItem) => {
      // skip any incomplete schedule items (not sure how this would even happen...)
      if (!scheduleItem.course_id) {
        return [];
      }

      // skip any schedule items that we don't have sufficient course information about.
      const course = courseMap[scheduleItem.course_id];
      if (!course || !course.sections) {
        return [];
      }

      const courseEvents = course.sections
        .filter((section) => {
          // consider only sections whose crn is in the list of selected crns
          if (!scheduleItem.selected_crns || !section || !section.crn) {
            return false;
          }
          return scheduleItem.selected_crns.includes(section.crn);
        })
        .flatMap((section) => {
          // get an array containing individual meeting times (there are multiple per section)
          if (!section?.meeting_times) {
            return [];
          }
          let values: { section: Sections; meetingTime: MeetingTimes }[] = [];
          for (let meetingTime of section?.meeting_times) {
            if (!meetingTime) {
              continue;
            }
            values.push({
              section: section,
              meetingTime: meetingTime,
            });
          }
          return values;
        })
        .filter((value) => {
          // discard any RRULES that can't be parsed (usually because value is "TBA")
          try {
            rrulestr(String(value.meetingTime.rrule));
          } catch (error) {
            return false;
          }

          // discard any meeting times which don't have a value duration
          if (!Number(value.meetingTime.duration)) {
            console.error("Invalid event duration", value.meetingTime.duration);
          }
          return true;
        })
        .map((value) => {
          // construct events consumable by FullCalendar
          const courseNameAbbr = `${course.subject} ${course.course_code}`;
          const sectionNameAbbr = `${value.section.section_type}${value.section.section_number}`;
          const color = scheduleItem.color
            ? scheduleItem.color
            : getDefaultColor();

          const event: Event = {
            title: `${courseNameAbbr} ${sectionNameAbbr}`,
            rrule: value.meetingTime.rrule as string,
            duration: {
              seconds: Number(value.meetingTime.duration),
            },
            backgroundColor: color,
            borderColor: getBorderColor(color),
          };
          return event;
        });
      return courseEvents;

      // filter out any null or undefined events
    })
    .filter((event) => !!event);
}

interface CourseCalendarProps {
  /* The schedule to display */
  schedule: Schedule | null;

  /* course data for the schedule to display */
  courses: CourseMap | null;
}

function CourseCalendar(props: CourseCalendarProps) {
  const [fullCalendarEvents, setFullCalendarEvents] = useState<Event[]>([]);

  // when the the schedule changes, rebuild calendar events
  useEffect(() => {
    if (!props.schedule || !props.courses) return;
    setFullCalendarEvents(
      buildFullCalendarEvents(props.schedule, props.courses)
    );
  }, [props.schedule, props.courses]);

  return (
    <div className="CourseCalendar">
      <FullCalendar
        defaultView="timeGridWeek"
        plugins={[rrulePlugin, timeGridPlugin]}
        events={fullCalendarEvents}
        minTime="08:00:00"
        weekends={false}
        allDaySlot={false}
        height="auto"
        eventTextColor="black"
      />
    </div>
  );
}

export default CourseCalendar;
