import { Box } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import React from "react";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";
import HeaderFooterProps from "./HeaderProps";

function Header(props: HeaderFooterProps) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("md"));

  return (
    <Grid container direction="column">
      <Grid item>
        {matches ? <HeaderDesktop {...props} /> : <HeaderMobile {...props} />}
      </Grid>
      <Box mt={props.noMargin ? 0 : 5}>
        <Grid item>{props.children}</Grid>
      </Box>
    </Grid>
  );
}

export { Header };
