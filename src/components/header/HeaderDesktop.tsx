import AppBar from "@material-ui/core/AppBar";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import IconButton from "@material-ui/core/IconButton";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import HelpIcon from "@material-ui/icons/Help";
import InfoIcon from "@material-ui/icons/Info";
import { useAuth } from "hooks/UseAuth";
import React from "react";
import { Link as RouterLink } from "react-router-dom";
import { BackButton } from "./BackButton";
import HeaderFooterProps from "./HeaderProps";
import UserButton from "./UserButton";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    navBarLink: {
      flexGrow: 1,
      paddingLeft: theme.spacing(1),
    },
    username: {
      paddingLeft: theme.spacing(1),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    toolbar: {
      display: "flex",
      justifyContent: "space-between",
      flexDirection: "row-reverse",
    },
  })
);

function HeaderDesktop(props: HeaderFooterProps) {
  const classes = useStyles();
  const auth = useAuth();

  let backButton = null;
  if (!props.hideBackButton) {
    if (!!auth.user) {
      backButton = <BackButton link="/dashboard" label={"Dashboard"} />;
    } else {
      backButton = <BackButton link="/" label="Home" />;
    }
  }

  return (
    <AppBar position="static" elevation={1}>
      <Toolbar variant="dense" className={classes.toolbar}>
        <ButtonGroup variant="text" aria-label="text primary button group">
          <IconButton component={RouterLink} to="/help" color="inherit">
            <HelpIcon />
            <Typography variant="subtitle1" className={classes.navBarLink}>
              Help
            </Typography>
          </IconButton>
          <IconButton component={RouterLink} to="/about" color="inherit">
            <InfoIcon />
            <Typography variant="subtitle1" className={classes.navBarLink}>
              About
            </Typography>
          </IconButton>
          <UserButton />
        </ButtonGroup>
        {backButton}
      </Toolbar>
    </AppBar>
  );
}

export default HeaderDesktop;
