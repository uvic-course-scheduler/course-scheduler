interface HeaderProps {
  /** Title to be displayed at the center of the header */
  title?: string;

  /** Should a back button be shown on this page? */
  hideBackButton?: boolean;

  /* True if there should be no margin under the header */
  noMargin?: boolean;

  /** Content to display under the header (automatic)*/
  children: any;
}

export default HeaderProps;
