import {
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import HelpIcon from "@material-ui/icons/Help";
import InfoIcon from "@material-ui/icons/Info";
import MenuIcon from "@material-ui/icons/Menu";
import SettingsIcon from "@material-ui/icons/Settings";
import { useAuth } from "hooks/UseAuth";
import React from "react";
import { Link as RouterLink, useHistory } from "react-router-dom";
import { BackButton } from "./BackButton";
import HeaderFooterProps from "./HeaderProps";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    help: {
      flexGrow: 1,
      paddingLeft: 5,
    },
    username: {
      paddingLeft: 5,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    toolbar: {
      display: "flex",
      justifyContent: "space-between",
      flexDirection: "row-reverse",
    },
    listItemCenter: {
      justifyContent: "center",
    },
    listItemTextCenter: {
      flexGrow: 0,
      minWidth: theme.spacing(7),
    },
  })
);

function HeaderMobile(props: HeaderFooterProps) {
  const classes = useStyles();
  const [drawer, setDrawer] = React.useState(false);
  const auth = useAuth();
  const history = useHistory();

  const toggleDrawer = () => {
    setDrawer(!drawer);
  };

  const handleDrawerClose = () => {
    setDrawer(false);
  };

  /**
   * Logs out the current user and returns to the login page.
   */
  async function onSignOutButtonClick() {
    auth.signout();
    history.push("/signin");
  }

  let backButton = null;
  if (!props.hideBackButton) {
    if (!!auth.user) {
      backButton = <BackButton link="/dashboard" label={"Dashboard"} />;
    } else {
      backButton = <BackButton link="/" label="Home" />;
    }
  }

  return (
    <React.Fragment>
      <AppBar position="static">
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={toggleDrawer}
          >
            <MenuIcon />
          </IconButton>
          {backButton}
        </Toolbar>
      </AppBar>
      <Drawer anchor="top" open={drawer}>
        <List>
          <ListItem button component={RouterLink} to="/help">
            <ListItemIcon>
              <HelpIcon />
            </ListItemIcon>
            <ListItemText>Help</ListItemText>
          </ListItem>
          <ListItem button component={RouterLink} to="/about">
            <ListItemIcon>
              <InfoIcon />
            </ListItemIcon>
            <ListItemText>About</ListItemText>
          </ListItem>

          {!!auth.user ? (
            <>
              <ListItem button component={RouterLink} to="/settings">
                <ListItemIcon>
                  <SettingsIcon />
                </ListItemIcon>
                <ListItemText>Settings</ListItemText>
              </ListItem>
              <ListItem button onClick={onSignOutButtonClick}>
                <ListItemIcon>
                  <ExitToAppIcon />
                </ListItemIcon>
                <ListItemText>Log out</ListItemText>
              </ListItem>
            </>
          ) : (
            <>
              <ListItem button component={RouterLink} to="/signin">
                <ListItemIcon>
                  <ExitToAppIcon />
                </ListItemIcon>
                <ListItemText>Sign In</ListItemText>
              </ListItem>
            </>
          )}

          <Divider />
          <ListItem
            button
            className={classes.listItemCenter}
            onClick={handleDrawerClose}
          >
            <ListItemIcon>
              <ExpandLessIcon />
            </ListItemIcon>
            <ListItemText className={classes.listItemTextCenter}>
              Close
            </ListItemText>
          </ListItem>
        </List>
      </Drawer>
    </React.Fragment>
  );
}

export default HeaderMobile;
