import React from "react";
import { Link as RouterLink } from "react-router-dom";

import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Typography from "@material-ui/core/Typography";

interface BackButtonProps {
  /** Back button destination */
  link: string;

  /** label for back button */
  label?: string;

  /** element children to use as label (automatic) */
  children?: any;
}

function BackButton(props: BackButtonProps) {
  return (
    <IconButton component={RouterLink} to={props.link} color="inherit">
      <ArrowBackIcon />
      <Typography variant="subtitle1">
        {props.label || props.children}
      </Typography>
    </IconButton>
  );
}

export { BackButton };
