import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { useAuth } from "hooks/UseAuth";
import React from "react";
import { Link as RouterLink, useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) =>
  createStyles({
    username: {
      paddingLeft: theme.spacing(1),
    },
  })
);

function UserButton() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const history = useHistory();
  const auth = useAuth();

  /**
   * Logs out the current user and returns to the login page.
   */
  async function onSignOutButtonClick() {
    auth.signout();
    history.push("/signin");
  }

  const openMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  // don't render the user button if no user is signed in
  if (!auth.user) {
    return (
      <IconButton component={RouterLink} to="/signin" color="inherit">
        <ExitToAppIcon />
        <Typography variant="subtitle1" className={classes.username}>
          Sign In
        </Typography>
      </IconButton>
    );
  }

  return (
    <>
      <IconButton
        aria-label="account of current user"
        aria-controls="user-menu"
        aria-haspopup="true"
        color="inherit"
        onClick={openMenu}
      >
        <AccountCircle />
        <Typography variant="subtitle1" className={classes.username}>
          {auth.user.attributes.email}
        </Typography>
        <ArrowDropDownIcon />
      </IconButton>
      <Menu
        id="user-menu"
        anchorEl={anchorEl}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        transformOrigin={{ vertical: "top", horizontal: "center" }}
        open={Boolean(anchorEl)}
        onClose={closeMenu}
      >
        <MenuItem component={RouterLink} to="/settings">
          <Typography variant="button">Settings</Typography>
        </MenuItem>
        <MenuItem onClick={onSignOutButtonClick}>
          <Typography variant="button">Log out</Typography>
        </MenuItem>
      </Menu>
    </>
  );
}

export default UserButton;
