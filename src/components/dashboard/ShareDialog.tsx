import { Box, Grid, IconButton, Link, Paper, Tooltip } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import React from 'react';

interface ConfirmDialogProps {

  /* Id of schedule to display a share link for */
  scheduleId?: string;

  /* When true, the dialog is displayed. */
  open: boolean;

  /* State function used to show or hide the dialog */
  setOpen: (open: boolean) => void;
};

const ShareDialog = (props: ConfirmDialogProps) => {
  const [showLinkCopiedTooltip, setShowLinkCopiedTooltip] = React.useState(false);

  // share link not the same as the displayed text so we can have it work on 
  const shareLink = `${window.origin}/share/${props.scheduleId}`;

  if (!props.scheduleId) {
    console.error("Can't share schedule missing ID.")
    return null;
  }

  // when the copy button is pressed, show a little 
  function handleCopyButtonClick() {
    navigator.clipboard.writeText(shareLink).then(function () {
      setShowLinkCopiedTooltip(true);
      window.setTimeout(() => {
        setShowLinkCopiedTooltip(false);
      }, 1000);
    }, () => {
      console.error("Failed to write share link to clipboard!");
    });
  }

  return (
    <Dialog
      open={props.open}
      onClose={() => props.setOpen(false)}
      aria-labelledby="share-dialog"
    >
      <DialogTitle id="share-dialog">
        Your schedule can be viewed at the link below
      </DialogTitle>
      <DialogContent>
        <Paper variant="outlined">
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
          >
            <Box ml={2}>
              <Link href={shareLink}>{shareLink}</Link>
            </Box>
            <Tooltip
              title="Copied Link To Clipboard!"
              arrow placement="top"
              disableHoverListener
              disableFocusListener
              disableTouchListener
              open={showLinkCopiedTooltip}
            >
              <IconButton aria-label="delete" onClick={handleCopyButtonClick}>
                <FileCopyIcon />
              </IconButton>
            </Tooltip>
          </Grid>
        </Paper>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => props.setOpen(false)} color="default">
          Done
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ShareDialog;
