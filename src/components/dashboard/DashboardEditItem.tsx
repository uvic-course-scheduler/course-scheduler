import {
  Box,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Tooltip,
} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import DoneIcon from "@material-ui/icons/Done";
import { CreateScheduleInput } from "API";
import { API } from "aws-amplify";
import React from "react";
import { convertTermAbbreviation } from "utils/NameTranslation";
import * as mutations from "../../graphql/mutations";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    scheduleItem: {
      display: "flex",
      padding: theme.spacing(2),
    },
    scheduleNameInput: {
      flexGrow: 1,
      marginRight: theme.spacing(2),
    },
    termSelect: {
      flexGrow: 1,
      maxWidth: 300,

      marginRight: theme.spacing(2),
    },
    menuPaper: {
      maxHeight: 400,
    },
    oldItem: {
      color: theme.palette.text.secondary,
    },
  })
);

interface DashboardEditItemProps {
  /* Callback function for when the user is finished editing */
  onEditComplete: Function;
}

function DashboardEditItem(props: DashboardEditItemProps) {
  const classes = useStyles();

  const [name, setName] = React.useState("");
  function onNameChange(e: React.ChangeEvent) {
    e.persist();
    const target = e.target as HTMLInputElement;
    setName(target.value);
  }

  const [term, setTerm] = React.useState("");
  const handleTermChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setTerm(event.target.value as string);
  };

  async function handleSave(event: React.MouseEvent<HTMLButtonElement>) {
    // create a new schedule
    const input: CreateScheduleInput = {
      name: name,
      term: term,
      items: [],
    };

    await API.graphql({
      query: mutations.createSchedule,
      variables: {
        input: input,
      },
    });

    // let the parent component know that the schedule has been saved
    props.onEditComplete();
  }

  const currentTerms = ["202201", "202109"];

  const pastTerms = [
    "202101",
    "202009",
    "202005",
    "202001",
    "201909",
    "201905",
    "201901",
    "201809",
    "201805",
    "201801",
    "201709",
    "201705",
    "201701",
  ];

  return (
    <Paper className={classes.scheduleItem}>
      <Box display="flex" flexGrow={1}>
        <TextField
          className={classes.scheduleNameInput}
          label="Schedule Name"
          variant="outlined"
          onChange={onNameChange}
        />
        <FormControl variant="outlined" className={classes.termSelect}>
          <InputLabel id="term-select-label">Term</InputLabel>
          <Select
            labelId="term-select-label"
            id="demo-simple-select"
            value={term}
            onChange={handleTermChange}
            label="Term"
            MenuProps={{ classes: { paper: classes.menuPaper } }}
          >
            {currentTerms.map((currentTerm) => (
              <MenuItem value={currentTerm}>
                {convertTermAbbreviation(currentTerm)}
              </MenuItem>
            ))}

            {pastTerms.map((pastTerm) => (
              <MenuItem className={classes.oldItem} value={pastTerm}>
                {convertTermAbbreviation(pastTerm)}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>

      <Tooltip title="Save" arrow>
        <IconButton onClick={handleSave}>
          <DoneIcon />
        </IconButton>
      </Tooltip>

      <Tooltip title="Discard" arrow>
        <IconButton onClick={() => props.onEditComplete()}>
          <CloseIcon />
        </IconButton>
      </Tooltip>
    </Paper>
  );
}

export default DashboardEditItem;
