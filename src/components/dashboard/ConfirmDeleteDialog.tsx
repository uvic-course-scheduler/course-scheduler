import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import React from "react";

interface ConfirmDeleteDialogProps {
  /* The name of the schedule being deleted */
  scheduleName?: string;

  /* When true, the dialog is displayed. */
  open: boolean;

  /* State function used to show or hide the dialog */
  setOpen: (open: boolean) => void;

  /* Callback function for when the user clicks yes */
  onConfirm: () => void;
}

// Based on (https://javascript.plainenglish.io/creating-a-confirm-dialog-in-react-and-material-ui-3d7aaea1d799)
const ConfirmDeleteDialog = (props: ConfirmDeleteDialogProps) => {
  let dialogTitleContents = <>Delete Schedule?</>;
  if (props.scheduleName) {
    dialogTitleContents = <>Delete {props.scheduleName}?</>;
  }

  return (
    <Dialog
      open={props.open}
      onClose={() => props.setOpen(false)}
      aria-labelledby="confirm-dialog"
    >
      <DialogTitle id="confirm-dialog">{dialogTitleContents}</DialogTitle>
      <DialogContent>This action cannot be undone.</DialogContent>
      <DialogActions>
        <Button onClick={() => props.setOpen(false)}>Cancel</Button>
        <Button
          onClick={() => {
            props.setOpen(false);
            props.onConfirm();
          }}
          color="primary"
        >
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDeleteDialog;
