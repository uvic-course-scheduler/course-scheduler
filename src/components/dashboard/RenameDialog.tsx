import { TextField } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import React from "react";

interface ConfirmDialogProps {
  /* The name of the schedule being renamed */
  scheduleName?: string;

  /* When true, the dialog is displayed. */
  open: boolean;

  /* State function used to show or hide the dialog */
  setOpen: (open: boolean) => void;

  /* Callback function for when the user clicks yes */
  onRename: (newName: string) => void;
}

const RenameDialog = (props: ConfirmDialogProps) => {
  const initialValue = props.scheduleName ? props.scheduleName : "";
  const [renameInput, setRenameInput] = React.useState(initialValue);

  function handleRenameInputChange(event: React.ChangeEvent) {
    const target = event.target as HTMLInputElement;
    setRenameInput(target.value);
  }

  return (
    <Dialog
      open={props.open}
      onClose={() => props.setOpen(false)}
      aria-labelledby="confirm-dialog"
    >
      <DialogTitle id="confirm-dialog">Rename Schedule</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          label="Rename To"
          variant="outlined"
          value={renameInput}
          onChange={handleRenameInputChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={() => props.setOpen(false)}>Cancel</Button>
        <Button
          onClick={() => {
            if (renameInput === "") {
              return;
            }
            props.setOpen(false);
            props.onRename(renameInput);
          }}
          color="primary"
        >
          Rename
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default RenameDialog;
