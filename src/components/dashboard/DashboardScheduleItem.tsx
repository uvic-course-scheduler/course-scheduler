import API from "@aws-amplify/api";
import {
  Box,
  Divider,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Tooltip
} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CloudDownloadIcon from "@material-ui/icons/CloudDownload";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ShareIcon from "@material-ui/icons/Share";
import { DeleteScheduleInput } from "API";
import { useCachedCourses, useCachedSchedule } from "hooks/UseCachedSchedule";
import React from "react";
import { Link as RouterLink } from "react-router-dom";
import { renameSchedule } from "utils/APICalls";
import { convertTermAbbreviation } from "utils/NameTranslation";
import * as mutations from "../../graphql/mutations";
import ConfirmDeleteDialog from "./ConfirmDeleteDialog";
import RenameDialog from "./RenameDialog";
import ShareDialog from "./ShareDialog";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    scheduleItem: {
      display: "flex",
      padding: theme.spacing(2),
      "&:hover": {
        backgroundColor: theme.palette.action.hover,
      },
    },
    scheduleLink: {
      textDecoration: "inherit",
      color: "inherit",
    },
    scheduleTitle: {
      fontWeight: theme.typography.fontWeightRegular,
    },
    scheduleSubtitle: {
      fontWeight: theme.typography.fontWeightLight,
      color: theme.palette.text.secondary,
    },
  })
);

interface DashboardScheduleItemProps {
  /* The ID of the schedule to display */
  scheduleId: string;
}

function DashboardScheduleItem(props: DashboardScheduleItemProps) {
  const classes = useStyles();
  const schedule = useCachedSchedule(props.scheduleId);
  const courses = useCachedCourses(props.scheduleId);

  // HTML element the "more actions" element appears below. Set null to hide.
  const [menuAnchorEl, setMenuAnchorEl] = React.useState<null | HTMLElement>(
    null
  );

  // state for schedule share dialog visibility
  const [showShare, setShowShare] = React.useState(false);

  // state for rename dialog visibility
  const [showRename, setShowRename] = React.useState(false);

  async function handleRename(newName: string) {
    if (schedule) {
      await renameSchedule(schedule, newName);
    }
  }

  // state for delete dialog visibility
  const [showDeleteConfirm, setShowDeleteConfirm] = React.useState(false);

  if (!schedule) {
    return null;
  }

  async function handleDeleteConfirm() {
    if (!schedule || !schedule.id) return;
    const deleteScheduleInput: DeleteScheduleInput = {
      id: schedule.id,
    };
    await API.graphql({
      query: mutations.deleteSchedule,
      variables: { input: deleteScheduleInput },
    });
  }

  // TODO: the dropdown menu should be split into it's own component
  const termName = convertTermAbbreviation(schedule.term as String);
  const courseCount = courses ? Object.keys(courses).length : "?";
  return (
    <Paper elevation={2} className={classes.scheduleItem}>
      <Box flexGrow={1}>
        <RouterLink
          to={`/schedule/${schedule.id}`}
          className={classes.scheduleLink}
        >
          <Typography className={classes.scheduleTitle} variant="subtitle1">
            {schedule.name}
          </Typography>
          <Typography className={classes.scheduleSubtitle} variant="subtitle2">
            {termName} &nbsp; | &nbsp; {courseCount} {(courseCount == 1) ? "course" : "courses"}
          </Typography>
        </RouterLink>
      </Box>

      <Tooltip title="More Actions" arrow>
        <IconButton
          aria-controls="schedule-item-menu"
          aria-haspopup="true"
          onClick={(event) => setMenuAnchorEl(event.currentTarget)}
        >
          <MoreVertIcon />
        </IconButton>
      </Tooltip>

      <Menu
        id="schedule-item-menu"
        anchorEl={menuAnchorEl}
        keepMounted
        open={Boolean(menuAnchorEl)}
        onClose={() => setMenuAnchorEl(null)}
      >
        <MenuItem
          onClick={() => {
            setShowShare(true);
            setMenuAnchorEl(null);
          }}
        >
          <ListItemIcon>
            <ShareIcon fontSize="small" />
          </ListItemIcon>
          <Typography>Share</Typography>
        </MenuItem>

        <MenuItem
          onClick={() => {
            setMenuAnchorEl(null);
          }}
        >
          <ListItemIcon>
            <CloudDownloadIcon fontSize="small" />
          </ListItemIcon>
          <Typography>Export</Typography>
        </MenuItem>

        <MenuItem
          onClick={() => {
            setShowRename(true);
            setMenuAnchorEl(null);
          }}
        >
          <ListItemIcon>
            <EditIcon fontSize="small" />
          </ListItemIcon>
          <Typography>Rename</Typography>
        </MenuItem>

        <MenuItem
          onClick={() => {
            setMenuAnchorEl(null);
          }}
        >
          <ListItemIcon>
            <FileCopyIcon fontSize="small" />
          </ListItemIcon>
          <Typography>Clone</Typography>
        </MenuItem>

        <Divider />
        <MenuItem
          onClick={() => {
            setShowDeleteConfirm(true);
            setMenuAnchorEl(null);
          }}
        >
          <ListItemIcon>
            <DeleteIcon fontSize="small" />
          </ListItemIcon>
          <Typography>Delete</Typography>
        </MenuItem>
      </Menu>

      <ShareDialog
        scheduleId={schedule.id}
        open={showShare}
        setOpen={setShowShare}
      />
      <RenameDialog
        scheduleName={schedule.name}
        open={showRename}
        setOpen={setShowRename}
        onRename={handleRename}
      />
      <ConfirmDeleteDialog
        scheduleName={schedule.name}
        open={showDeleteConfirm}
        setOpen={setShowDeleteConfirm}
        onConfirm={handleDeleteConfirm}
      />
    </Paper>
  );
}

export default DashboardScheduleItem;
