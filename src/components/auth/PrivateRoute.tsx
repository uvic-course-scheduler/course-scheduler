import { useAuth } from "hooks/UseAuth";
import React from "react";
import { Redirect, Route } from "react-router-dom";

interface PrivateRouteProps {
  /* Router path to display the child component under (if authenticated) */
  path: string;

  /* Content to display under the header (automatic)*/
  children: any;
}

/**
 * A wrapper for <Route> that redirects to the login screen if you're not yet authenticated.
 * (see: https://reactrouter.com/web/example/auth-workflow)
 */
function PrivateRoute(props: PrivateRouteProps) {
  const auth = useAuth();

  // if we don't yet know whether there is a logged in user, render a blank screen
  if (auth.authPending) {
    return null;
  }

  return (
    <Route
      exact
      path={props.path}
      render={({ location }) =>
        !!auth.user ? (
          props.children
        ) : (
          <Redirect
            to={{
              pathname: "/signin",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;
