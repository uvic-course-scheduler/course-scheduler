import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import React, { useState } from "react";
import { Link as RouterLink } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));


interface SignInFormProps {
  onSubmit: (username: string, password: string) => void;

  submitError: boolean;

  submitDisabled: boolean;
}

function SignInForm(props: SignInFormProps) {
  const classes = useStyles();

  function onFormFieldChange(e: React.ChangeEvent) {
    e.persist();
    const target = e.target as HTMLInputElement;
    setFormState({ ...formState, [target.name]: target.value });
  }

  const [formState, setFormState] = useState({
    email: "",
    password: "",
  });

  const [formErrorState, setFromErrorState] = useState({
    email: false,
    password: false,
  });

  return (
    <>
      <TextField
        error={formErrorState.email || props.submitError}
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="email"
        label="Email Address"
        name="email"
        autoComplete="email"
        onChange={onFormFieldChange}
      />
      <TextField
        error={formErrorState.password || props.submitError}
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
        onChange={onFormFieldChange}
      />
      <Button
        fullWidth
        variant="contained"
        color="primary"
        className={classes.submit}
        disabled={props.submitDisabled}
        onClick={() => {

          // make sure both fields are filled
          if (!formState.email || !formState.password) {
            setFromErrorState({
              ...formErrorState,
              email: !formState.email,
              password: !formState.password
            })
            return;
          }

          // since there is text in both the input fields, try to sign in
          props.onSubmit(formState.email, formState.password);
        }}
      >
        Sign In
      </Button>
      <Grid container>
        <Grid item xs>
          <Link
            component={RouterLink}
            to={"/forgotpassword"}
            variant="body2"
          >
            Forgot password?
          </Link>
        </Grid>
        <Grid item>
          <Link component={RouterLink} to={"/signup"} variant="body2">
            Don't have an account? Sign Up
          </Link>
        </Grid>
      </Grid>
    </>
  )
}

export default SignInForm;
