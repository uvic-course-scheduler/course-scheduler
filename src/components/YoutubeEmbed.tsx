import { makeStyles } from "@material-ui/core/styles";
import React from "react";

interface YoutubeEmbedProps {
  embedId: string;
}

// I stole these responsive styles from (https://www.w3schools.com/howto/howto_css_responsive_iframes.asp)
const useStyles = makeStyles((theme) => ({
  embedContainer: {
    marginTop: theme.spacing(3),
    position: "relative",
    width: "100%",
    overflow: "hidden",
    paddingTop: "56.25%" /* 16:9 Aspect Ratio */
  },
  embedIframe: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: "100%",
    height: "100%",
    border: "none"
  }
}));

/**
 * Youtube embed IFrame reference: (https://developers.google.com/youtube/iframe_api_reference)
 */
function YoutubeEmbed(props: YoutubeEmbedProps) {
  const classes = useStyles();

  return (
    <div className={classes.embedContainer}>
      <iframe
        className={classes.embedIframe}
        width="100%"
        height="1em"
        src={`https://www.youtube-nocookie.com/embed/${props.embedId}`}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        title="Embedded youtube"
      />
    </div>
  )
}

export default YoutubeEmbed;
