import { ClickAwayListener, IconButton, Popper } from "@material-ui/core";
import LensIcon from "@material-ui/icons/Lens";
import React, { useState } from "react";
import { GithubPicker } from "react-color";
import { PALETTE } from "utils/CourseColorPalette";

interface CourseColorPickerProps {
  /* The currently selected color */
  color: string;

  /* Callback function for when a new color is selected */
  onColorChange: (newColor: string) => void;
}

function CourseColorPicker(props: CourseColorPickerProps) {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleIconClick = (event: any) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
    event.stopPropagation();
  };

  const handleColorChangeComplete = (newColor: any, event: any) => {
    // The problem: when the user types 3 characters into the custom hex input, it fires ColorChangeComplete with the three characters they entred filled out
    // to a full hex number (if they entered "abc" then it fires with newColor.hex being "#aabbcc")
    setAnchorEl(null);

    if (newColor !== props.color) {
      props.onColorChange(newColor.hex);
    }
  };

  const handleClickAway = () => {
    if (open) {
      setAnchorEl(null);
    }
  };

  const open = Boolean(anchorEl);
  const id = open ? "color-popper" : undefined;

  return (
    <div>
      <IconButton onClick={handleIconClick} size="small">
        <LensIcon style={{ color: props.color }} />
      </IconButton>
      <Popper
        id={id}
        open={open}
        anchorEl={anchorEl}
        placement="bottom-start"
        onClick={(event: any) => {
          event.stopPropagation();
        }}
      >
        <ClickAwayListener onClickAway={handleClickAway}>
          <GithubPicker
            onChangeComplete={handleColorChangeComplete}
            onChange={(newColor: any, event: any) => {
              event.stopPropagation();
            }}
            triangle="hide"
            colors={PALETTE}
            width="162px"
          />
        </ClickAwayListener>
      </Popper>
    </div>
  );
}

export default CourseColorPicker;
