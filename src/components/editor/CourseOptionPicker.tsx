import { Box, Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelActions from "@material-ui/core/ExpansionPanelActions";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Course, Schedule, ScheduleItem, Sections } from "API";
import React, { useEffect, useState } from "react";
import { updateScheduleItemColor } from "utils/APICalls";
import { getDefaultColor } from "utils/CourseColorPalette";
import { getCourseSearchLink } from "utils/NameTranslation";
import CourseColorPicker from "./CourseColorPicker";
import SectionPicker from "./SectionPicker";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      fontWeight: theme.typography.fontWeightMedium,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
      marginLeft: theme.spacing(1),
    },
    panelDetails: {
      flexDirection: "column",
    },
  })
);

enum SectionType {
  LECTURE,
  LAB,
  TUTORIAL,
}

interface CourseOptionPickerProps {
  /* The item in the schedule to display */
  scheduleItem: ScheduleItem;

  /* The schedule this item belongs in */
  schedule: Schedule;

  /* The course to display */
  course: Course;

  /* Callback function for when the 'remove' button is pressed */
  onRemove: Function;

  /* Callback function for when the schedule has been updated e.g. a new section selected */
  onCrnSelectionChange: (courseId: string, crns: string[]) => void;
}

interface SectionOptions {
  [SectionType.LECTURE]: Sections[];
  [SectionType.LAB]: Sections[];
  [SectionType.TUTORIAL]: Sections[];
}

interface SelectedCrns {
  [SectionType.LECTURE]: string;
  [SectionType.LAB]: string;
  [SectionType.TUTORIAL]: string;
}

function CourseOptionPicker(props: CourseOptionPickerProps) {
  const classes = useStyles();

  const [sectionOptions, setSectionOptions] = useState<SectionOptions>({
    [SectionType.LECTURE]: [],
    [SectionType.LAB]: [],
    [SectionType.TUTORIAL]: [],
  });

  const [selectedCrns, setSelectedCrns] = useState<SelectedCrns>({
    [SectionType.LECTURE]: "",
    [SectionType.LAB]: "",
    [SectionType.TUTORIAL]: "",
  });

  // when the schedule item changes
  useEffect(() => {
    async function fetchCourseInfo() {
      // update schedule Options
      if (!props.course.sections) {
        return;
      }

      const nonNullSections = props.course.sections.filter((section) => {
        return (
          !!section &&
          !!section.section_type &&
          !!section.section_number &&
          !!section.crn
        );
      }) as Sections[];

      const newSectionOptions = {
        [SectionType.LECTURE]: nonNullSections.filter(
          (section) => section.section_type === "A"
        ),
        [SectionType.LAB]: nonNullSections.filter(
          (section) => section.section_type === "B"
        ),
        [SectionType.TUTORIAL]: nonNullSections.filter(
          (section) => section.section_type === "T"
        ),
      };
      setSectionOptions(newSectionOptions);

      // update selected CRNs
      let scheduleItemCrns = props.scheduleItem.selected_crns
        ? props.scheduleItem.selected_crns
        : [];

      function getSelectedCrnBySectionType(sectionType: SectionType) {
        let selectedCrn = scheduleItemCrns.filter((crn) => {
          return newSectionOptions[sectionType]
            .map((section) => section.crn)
            .includes(crn);
        });
        return selectedCrn.length > 0 ? selectedCrn[0] : "";
      }

      let newSelectedCrns = {
        [SectionType.LECTURE]: getSelectedCrnBySectionType(SectionType.LECTURE),
        [SectionType.LAB]: getSelectedCrnBySectionType(SectionType.LAB),
        [SectionType.TUTORIAL]: getSelectedCrnBySectionType(
          SectionType.TUTORIAL
        ),
      };

      setSelectedCrns(newSelectedCrns);
    }
    fetchCourseInfo();
  }, [props.course.sections, props.scheduleItem]);

  /**
   * Callback function for when a section is selected or changed.
   *
   * @param sectionType The category of section that a section was selected for.
   * @param newCrn The CRN of the selected section.
   */
  function handleSectionSelect(sectionType: SectionType, newCrn: string): void {
    const newSelectedCrns = { ...selectedCrns };
    newSelectedCrns[sectionType] = newCrn;
    setSelectedCrns(newSelectedCrns);

    // tell the parent component about the change so that it can be persisted
    if (!props.scheduleItem.course_id) throw Error("Missing course id");

    let crns: string[] = [
      newSelectedCrns[SectionType.LECTURE],
      newSelectedCrns[SectionType.LAB],
      newSelectedCrns[SectionType.TUTORIAL],
    ].filter((crn) => !!crn);

    props.onCrnSelectionChange(props.scheduleItem.course_id, crns);
  }

  return (
    <ExpansionPanel defaultExpanded={false}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Grid container direction="row" alignItems="center">
          <Grid item>
            <CourseColorPicker
              color={
                props.scheduleItem.color
                  ? props.scheduleItem.color
                  : getDefaultColor()
              }
              onColorChange={(newColor: string) => {
                console.log("Changing color to", newColor);
                if (props.course.id) {
                  updateScheduleItemColor(
                    props.schedule,
                    props.course.id,
                    newColor
                  );
                }
              }}
            />
          </Grid>
          <Grid item>
            <Box flexShrink={0} ml={1}>
              <Typography className={classes.heading}>
                {props.course.subject} {props.course.course_code}
              </Typography>
            </Box>
          </Grid>
          <Grid item>
            <Typography className={classes.secondaryHeading}>
              {props.course.course_title}
            </Typography>
          </Grid>
        </Grid>
      </ExpansionPanelSummary>

      <ExpansionPanelDetails className={classes.panelDetails}>
        <SectionPicker
          label="Lecture Section"
          sections={sectionOptions[SectionType.LECTURE]}
          selectedCrn={selectedCrns[SectionType.LECTURE]}
          onSelectedCrnChange={(newCrn) =>
            handleSectionSelect(SectionType.LECTURE, newCrn)
          }
        />
        <SectionPicker
          label="Lab Section"
          sections={sectionOptions[SectionType.LAB]}
          selectedCrn={selectedCrns[SectionType.LAB]}
          onSelectedCrnChange={(newCrn) =>
            handleSectionSelect(SectionType.LAB, newCrn)
          }
        />
        <SectionPicker
          label="Tutorial Section"
          sections={sectionOptions[SectionType.TUTORIAL]}
          selectedCrn={selectedCrns[SectionType.TUTORIAL]}
          onSelectedCrnChange={(newCrn) =>
            handleSectionSelect(SectionType.TUTORIAL, newCrn)
          }
        />
      </ExpansionPanelDetails>
      <Divider />
      <ExpansionPanelActions>
        <Button
          size="small"
          variant="outlined"
          href={getCourseSearchLink(props.course)}
          target="_blank"
        >
          View on uvic.ca
        </Button>
        <Button size="small" onClick={() => props.onRemove()}>
          Remove
        </Button>
      </ExpansionPanelActions>
    </ExpansionPanel>
  );
}

export default CourseOptionPicker;
