import {
  Box,
  Button,
  ButtonGroup,
  CircularProgress,
  createStyles,
  IconButton,
  makeStyles,
  Theme,
  Tooltip,
  Typography,
} from "@material-ui/core";
import FullscreenIcon from "@material-ui/icons/Fullscreen";
import FullscreenExitIcon from "@material-ui/icons/FullscreenExit";
import { ScheduleItemInput } from "API";
import CourseOptionPicker from "components/editor/CourseOptionPicker";
import CourseSearch from "components/editor/CourseSearch";
import { useCachedCourses, useCachedSchedule } from "hooks/UseCachedSchedule";
import React from "react";
import { Link as RouterLink } from "react-router-dom";
import {
  addCourseToSchedule,
  removeCourseFromSchedule,
  updateScheduleItems,
} from "utils/APICalls";
import { convertTermAbbreviation } from "utils/NameTranslation";

interface ScheduleEditorProps {
  /* The ID of the schedule to edit */
  scheduleId: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    secondaryHeading: {
      color: theme.palette.text.secondary,
    },
    expandButton: {
      padding: 0,
      paddingLeft: theme.spacing(1),
    },
    closeButton: {
      padding: 0,
    },
  })
);

function ScheduleEditor(props: ScheduleEditorProps) {
  const classes = useStyles();
  const schedule = useCachedSchedule(props.scheduleId);
  const courses = useCachedCourses(props.scheduleId);

  if (!schedule) {
    return null;
  }

  // create a list of already-selected id's to disable in the course search bar
  let disabledOptionIds: string[] = [];
  if (schedule && schedule.items) {
    disabledOptionIds = schedule.items
      .map((item) => (item ? item.course_id : null))
      .filter((item) => !!item) as string[];
  }

  async function handleCrnSelectionChange(courseId: string, crns: string[]) {
    if (!schedule) {
      throw Error("Tried to update schedule when schedule not yet loaded");
    } else if (!schedule.items) {
      throw Error("Schedule missing items list");
    }

    let items: ScheduleItemInput[] = schedule.items.map((item) => {
      if (item.course_id === undefined || item.selected_crns === undefined) {
        throw Error("Encountered incomplete schedule item");
      }
      return {
        course_id: item.course_id,
        selected_crns: item.course_id === courseId ? crns : item.selected_crns,
        color: item.color,
      };
    });

    updateScheduleItems(schedule, items);
  }

  async function handleCourseSelect(id: string) {
    if (schedule) addCourseToSchedule(schedule, id);
  }

  async function handleCourseRemove(id: string | undefined) {
    if (schedule && id) removeCourseFromSchedule(schedule, id);
  }

  // render a loading spinner if the schedule has not loaded yet
  if (!schedule || !courses) {
    return (
      <Box mt={10} textAlign="center">
        <CircularProgress disableShrink />
      </Box>
    );
  } else {
    return (
      <>
        <Typography variant="h4">{schedule.name}</Typography>
        <Typography variant="subtitle1" className={classes.secondaryHeading}>
          {convertTermAbbreviation(schedule.term as string)}
        </Typography>
        <Box mt={2}>
          <ButtonGroup fullWidth>
            <Button component={RouterLink} to={`/register/${props.scheduleId}`}>
              Register
            </Button>
            <Button>Generate</Button>
          </ButtonGroup>
        </Box>
        <Box mt={2}>
          <Typography variant="h6">Add Course</Typography>
          <CourseSearch
            term={schedule.term}
            onCourseSelect={handleCourseSelect}
            disabledOptionIds={disabledOptionIds}
          />
        </Box>
        <Box mt={2}>
          <Box display="flex" mb={1}>
            <Box flexGrow={1}>
              <Typography variant="h6">Your Courses</Typography>
            </Box>
            <Tooltip title="Expand All" arrow>
              <IconButton
                aria-label="Expand All"
                className={classes.expandButton}
              >
                <FullscreenIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Close All" arrow>
              <IconButton
                aria-label="Close All"
                className={classes.closeButton}
              >
                <FullscreenExitIcon />
              </IconButton>
            </Tooltip>
          </Box>
          {schedule.items &&
            schedule.items.map(
              (item) =>
                item.course_id &&
                courses.hasOwnProperty(item.course_id) && (
                  <CourseOptionPicker
                    key={item.course_id}
                    scheduleItem={item}
                    schedule={schedule}
                    course={courses[item.course_id]}
                    onRemove={() => handleCourseRemove(item.course_id)}
                    onCrnSelectionChange={handleCrnSelectionChange}
                  />
                )
            )}
        </Box>
      </>
    );
  }
}

export default ScheduleEditor;
