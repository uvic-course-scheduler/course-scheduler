import {
  FormControl,
  InputLabel,
  MenuItem, Select
} from "@material-ui/core";
import { Sections } from "API";
import React from "react";

interface SectionPickerProps {
  label: string;
  sections: Sections[];
  selectedCrn: string;
  onSelectedCrnChange: (newCRN: string) => void;
}

/* Selector for lecture/lab/tutorial sections */
function SectionPicker(props: SectionPickerProps) {
  const handleSectionChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    props.onSelectedCrnChange(event.target.value as string);
  };

  // render nothing if there are no sections to select from
  if (props.sections.length === 0) {
    return null;
  }

  return (
    <FormControl fullWidth={true}>
      <InputLabel>
        {props.label}
      </InputLabel>
      <Select value={props.selectedCrn} onChange={handleSectionChange}>
        {
          props.sections.map((section) =>
            <MenuItem key={section.crn as string} value={section.crn as string}>
              {section.section_type}{section.section_number}
            </MenuItem>
          )
        }
      </Select>
    </FormControl>
  );
}

export default SectionPicker;
