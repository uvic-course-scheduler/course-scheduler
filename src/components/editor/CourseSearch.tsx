import API, { GraphQLResult } from "@aws-amplify/api";
import { Grid, InputAdornment, Typography } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Course, CoursesByTermQuery } from "API";
import React, { useEffect, useState } from "react";

interface CourseSearchProps {
  /* The term to find courses in. When missing, the search is disabled. */
  term?: string;

  /* Callback fired when a course is selected.  */
  onCourseSelect: (id: string) => void;

  /* ID's of courses to disable / "grey out" */
  disabledOptionIds: string[];
}

// TODO: Speed up by virtualizing list with react-window? (~1000 courses is a lot...)
// (see: https://material-ui.com/components/autocomplete/#virtualization)
function CourseSearch(props: CourseSearchProps) {
  const [courseOptions, setCourseOptions] = useState<any[]>([]);

  // get course options when the schedule info has loaded
  useEffect(() => {
    // skip loading if the schedule hasn't been loaded yet
    if (!props.term) {
      return;
    }

    /**
     * Load course data in chunks of 100 courses. There are typically < 1000 courses offered
     * in a given term, so the total data transferred is under 100kB
     */
    async function fetchCourseOptions() {
      let allCourses: Course[] = [];
      let nextToken: string | null | undefined = null;
      do {
        const query = `
          query CoursesByTerm {
            coursesByTerm(
              term: "${props.term}"
              nextToken: ${nextToken ? `"${nextToken}"` : null}
            ) {
              items {
                subject
                course_code
                course_title
                id
              }
              nextToken
            }
          }        
        `;
        const result = (await API.graphql({
          query: query,
        })) as GraphQLResult<CoursesByTermQuery>;
        const courses = result.data?.coursesByTerm?.items;
        allCourses.push.apply(allCourses, courses as Course[]);
        nextToken = result.data?.coursesByTerm?.nextToken;
      } while (nextToken);

      // sort courses by subject then code
      if (allCourses) {
        allCourses.sort((a: Course, b: Course) => {
          // sort first by subject
          if (!a.subject || !b.subject) {
            return 0;
          } else if (a.subject > b.subject) {
            return 1;
          } else if (a.subject < b.subject) {
            return -1;
          } else {
            // when subjects are equal, sort next by course code
            if (!a.course_code || !b.course_code) {
              return 0;
            } else if (a.course_code > b.course_code) {
              return 1;
            } else if (a.course_code < b.course_code) {
              return -1;
            } else {
              return 0;
            }
          }
        });
      }
      setCourseOptions(allCourses);
    }
    fetchCourseOptions();
  }, [props.term]);

  // I'm using a key here as a hacky way to clear the search bar after selecting something
  // (see: https://stackoverflow.com/questions/59790956/material-ui-autocomplete-clear-value)
  const [resetKey, setResetKey] = React.useState(0);

  return (
    <Autocomplete
      forcePopupIcon={false}
      disableClearable
      loading={courseOptions.length === 0}
      loadingText="Loading Course Options..."
      noOptionsText="No Courses Found"
      options={courseOptions}
      groupBy={(option) => option.subject}
      getOptionLabel={(option) => `${option.subject} ${option.course_code}`}
      getOptionDisabled={(option) =>
        props.disabledOptionIds.includes(option.id)
      }
      renderInput={(params) => (
        <TextField
          {...params}
          placeholder="Search for your course"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
      )}
      fullWidth
      onChange={(event, value, reason) => {
        if (reason === "select-option") {
          props.onCourseSelect(value.id);
          setResetKey(Math.random());
        }
      }}
      key={resetKey}
      renderOption={(option) => (
        <Grid container direction="column">
          <Typography variant="body2" style={{ fontWeight: 500 }}>
            {`${option.subject} ${option.course_code}`}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            {option.course_title}
          </Typography>
        </Grid>
      )}
    />
  );
}

export default CourseSearch;
