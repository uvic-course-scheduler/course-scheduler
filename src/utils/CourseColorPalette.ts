const PALETTE: string[] = [
  "#dc2026",
  "#ff887c",
  "#ffb878",
  "#fbd75b",
  "#7ae7bf",
  "#50b748",
  "#45d6db",
  "#5384ed",
  "#a4bdfc",
  "#dbadff",
  "#e1e1e1",
  "#9fc6e7",
];

function getDefaultColor(): string {
  return PALETTE[0];
}

function getColorByIndex(index: number): string {
  return PALETTE[index % PALETTE.length];
}

function getBorderColor(color: string): string {
  //return tinycolor(color).darken(5).toString()
  return color;
}

export { PALETTE, getDefaultColor, getColorByIndex, getBorderColor };
