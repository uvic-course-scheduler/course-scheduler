import { getICalString } from "./ICalConverter";
import { sampleCourses, sampleSchedule } from "./ICalConverterTestData";

test("adds 1 + 2 to equal 3", () => {
  expect(1 + 2).toBe(3);
});

test("returns a string", () => {
  let iCalendarString = getICalString(sampleSchedule, sampleCourses);
});
