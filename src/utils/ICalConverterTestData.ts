import { Course, Schedule } from "API";

const sampleSchedule: Schedule = {
  __typename: "Schedule",
  updatedAt: "2021-05-11T07:47:32.461Z",
  term: "202105",
  owner: "720d2628-ec17-4fe5-9adf-908f82b248be",
  name: "Test Schedule",
  items: [
    {
      __typename: "ScheduleItem",
      selected_crns: ["32102", "32103"],
      course_id: "0179f9a9-ab57-4983-a593-e2227890505b",
    },
    {
      __typename: "ScheduleItem",
      selected_crns: ["32641", "32642"],
      course_id: "a6bb070e-d4b1-45f8-81a2-79185be8ae29",
    },
  ],
  id: "c37d2081-b009-40a9-89cb-1fa581e98813",
  createdAt: "2021-05-08T20:42:37.739Z",
};

const sampleCourses: Course[] = [
  {
    __typename: "Course",
    id: "0179f9a9-ab57-4983-a593-e2227890505b",
    course_code: "109",
    course_title: "Introduction to Calculus",
    createdAt: "2021-04-24T11:59:04.044Z",
    sections: [
      {
        __typename: "Sections",
        section_type: "A",
        section_number: "01",
        schedule_type_alt: "Lecture Schedule Type",
        reg_start: "2021-03-15T00:00:00",
        reg_end: "2021-05-20T00:00:00",
        method: "Online Instructional Method",
        meeting_times: [
          {
            __typename: "MeetingTimes",
            type: "Every Week",
            start_time: "10:30:00",
            schedule_type: "Lecture",
            rrule:
              "DTSTART:20210505T103000 RRULE:FREQ=WEEKLY;WKST=SU;UNTIL=20210730T000000;BYDAY=TU,WE,FR",
            location: "TBA",
            instructors: [
              {
                __typename: "Instructors",
                name: "Junling Ma",
                email: "junlingm@uvic.ca",
              },
            ],
            end_time: "11:20:00",
            duration: "3000",
          },
        ],
        description: "This course will be offered fully online and synchronous",
        crn: "32102",
        credits: "1.500 Credits",
        campus: "Online Campus",
        attributes: "Undergrad Course - 1.5 units",
      },
      {
        __typename: "Sections",
        section_type: "T",
        section_number: "01",
        schedule_type_alt: "Tutorial Schedule Type",
        reg_start: "2021-03-15T00:00:00",
        reg_end: "2021-05-20T00:00:00",
        method: "Online Instructional Method",
        meeting_times: [
          {
            __typename: "MeetingTimes",
            type: "Every Week",
            start_time: "11:30:00",
            schedule_type: "Tutorial",
            rrule:
              "DTSTART:20210510T113000 RRULE:FREQ=WEEKLY;WKST=SU;UNTIL=20210730T000000;BYDAY=TH",
            location: "TBA",
            instructors: [
              {
                __typename: "Instructors",
                name: "TBA",
                email: "TBA",
              },
            ],
            end_time: "12:20:00",
            duration: "3000",
          },
        ],
        description: "This course will be offered fully online and synchronous",
        crn: "32103",
        credits: "0.000 Credits",
        campus: "Online Campus",
        attributes: "",
      },
      {
        __typename: "Sections",
        section_type: "T",
        section_number: "02",
        schedule_type_alt: "Tutorial Schedule Type",
        reg_start: "2021-03-15T00:00:00",
        reg_end: "2021-05-20T00:00:00",
        method: "Online Instructional Method",
        meeting_times: [
          {
            __typename: "MeetingTimes",
            type: "Every Week",
            start_time: "10:30:00",
            schedule_type: "Tutorial",
            rrule:
              "DTSTART:20210510T103000 RRULE:FREQ=WEEKLY;WKST=SU;UNTIL=20210730T000000;BYDAY=TH",
            location: "TBA",
            instructors: [
              {
                __typename: "Instructors",
                name: "TBA",
                email: "TBA",
              },
            ],
            end_time: "11:20:00",
            duration: "3000",
          },
        ],
        description: "This course will be offered fully online and synchronous",
        crn: "32104",
        credits: "0.000 Credits",
        campus: "Online Campus",
        attributes: "",
      },
    ],
    updatedAt: "2021-04-30T10:35:18.668Z",
    term: "202105",
    subject: "MATH",
    levels: "Law, Undergraduate",
  },
  {
    __typename: "Course",
    id: "a6bb070e-d4b1-45f8-81a2-79185be8ae29",
    course_code: "184",
    course_title: "Evolution and Biodiversity",
    createdAt: "2021-04-24T11:58:16.153Z",
    sections: [
      {
        __typename: "Sections",
        section_type: "A",
        section_number: "01",
        schedule_type_alt: "Lecture Schedule Type",
        reg_start: "2021-03-15T00:00:00",
        reg_end: "2021-05-17T00:00:00",
        method: "Online Instructional Method",
        meeting_times: [
          {
            __typename: "MeetingTimes",
            type: "Every Week",
            start_time: "08:30:00",
            schedule_type: "Lecture",
            rrule:
              "DTSTART:20210510T083000 RRULE:FREQ=WEEKLY;WKST=SU;UNTIL=20210625T000000;BYDAY=MO,WE,TH",
            location: "TBA",
            instructors: [
              {
                __typename: "Instructors",
                name: "David Punzalan",
                email: "davidpunzalan@uvic.ca",
              },
            ],
            end_time: "10:20:00",
            duration: "6600",
          },
        ],
        description:
          'This course will be offered fully online and blended ( a mix of "real-time" and asynchronous sessions.',
        crn: "32641",
        credits: "1.500 Credits",
        campus: "Online Campus",
        attributes: "Undergrad Course - 1.5 units",
      },
      {
        __typename: "Sections",
        section_type: "B",
        section_number: "01",
        schedule_type_alt: "Lab Schedule Type",
        reg_start: "2021-03-15T00:00:00",
        reg_end: "2021-05-17T00:00:00",
        method: "Online Instructional Method",
        meeting_times: [
          {
            __typename: "MeetingTimes",
            type: "Every Week",
            start_time: "11:30:00",
            schedule_type: "Lab",
            rrule:
              "DTSTART:20210510T113000 RRULE:FREQ=WEEKLY;WKST=SU;UNTIL=20210625T000000;BYDAY=MO,WE",
            location: "TBA",
            instructors: [
              {
                __typename: "Instructors",
                name: "Katharine R. Hind",
                email: "khind@uvic.ca",
              },
            ],
            end_time: "14:20:00",
            duration: "10200",
          },
        ],
        description:
          "The lab will be offered fully online and blended (a mix of “real-time” and asynchronous sessions).",
        crn: "32642",
        credits: "0.000 Credits",
        campus: "Online Campus",
        attributes: "",
      },
      {
        __typename: "Sections",
        section_type: "B",
        section_number: "02",
        schedule_type_alt: "Lab Schedule Type",
        reg_start: "2021-03-15T00:00:00",
        reg_end: "2021-05-17T00:00:00",
        method: "Online Instructional Method",
        meeting_times: [
          {
            __typename: "MeetingTimes",
            type: "Every Week",
            start_time: "14:30:00",
            schedule_type: "Lab",
            rrule:
              "DTSTART:20210510T143000 RRULE:FREQ=WEEKLY;WKST=SU;UNTIL=20210625T000000;BYDAY=MO,WE",
            location: "TBA",
            instructors: [
              {
                __typename: "Instructors",
                name: "Katharine R. Hind",
                email: "khind@uvic.ca",
              },
            ],
            end_time: "17:20:00",
            duration: "10200",
          },
        ],
        description:
          "The lab will be offered fully online and blended (a mix of “real-time” and asynchronous sessions).",
        crn: "33079",
        credits: "0.000 Credits",
        campus: "Online Campus",
        attributes: "",
      },
    ],
    updatedAt: "2021-04-30T10:33:59.893Z",
    term: "202105",
    subject: "BIOL",
    levels: "Law, Undergraduate",
  },
];

export { sampleCourses, sampleSchedule };
