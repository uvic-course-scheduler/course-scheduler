import { Course } from "API";

/**
 * Convert's one of UVic's term abbrevations to a nicer human-readable format.
 *
 * @param abbreviation A term abbreviation e.g. 202105
 */
function convertTermAbbreviation(abbreviation: String): String {
  const year = abbreviation.substring(0, 4);
  const month = abbreviation.substring(4);

  let term = "";
  switch (month) {
    case "01":
      term = "Spring (Jan - April)";
      break;
    case "05":
      term = "Summer (May - August)";
      break;
    case "09":
      term = "Fall (Sept - December)";
      break;
  }

  return `${term} ${year}`;
}

function getCourseSearchLink(course: Course) {
  const { term, subject, course_code } = course;

  if (!term || !subject || !course_code) {
    console.error("Incomplete course info", course);
  }

  return `https://www.uvic.ca/BAN1P/bwckctlg.p_disp_listcrse?term_in=${term}&subj_in=${subject}&crse_in=${course_code}&schd_in=`;
}

export { convertTermAbbreviation, getCourseSearchLink };
