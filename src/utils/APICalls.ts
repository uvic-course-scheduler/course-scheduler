import API, { GraphQLResult } from "@aws-amplify/api";
import {
  Course,
  GetCourseQuery,
  GetScheduleQuery,
  Schedule,
  ScheduleItemInput,
  UpdateScheduleInput,
  UpdateScheduleMutation,
} from "API";
import { graphqlOperation } from "aws-amplify";
import Observable from "zen-observable-ts";
import * as mutations from "../graphql/mutations";
import * as queries from "../graphql/queries";
import * as subscriptions from "../graphql/subscriptions";
import { getColorByIndex } from "./CourseColorPalette";

// TODO: this file is horrifying

async function getScheduleById(id: string) {
  const result = (await API.graphql({
    query: queries.getSchedule,
    variables: {
      id: id,
    },
  })) as GraphQLResult<GetScheduleQuery>;
  const schedule = result.data?.getSchedule;
  return schedule;
}

async function getSchedulesForUser(username: string) {
  const allSchedules = await (API.graphql({
    query: queries.schedulesByOwner,
    variables: {
      owner: username,
    },
  }) as Promise<GraphQLResult<any>>);
  return allSchedules.data.schedulesByOwner.items as Schedule[];
}

// TODO: using this function returns all the fields of a course, but that is rarely needed.
async function getCourseById(id: string) {
  const result = (await API.graphql({
    query: queries.getCourse,
    variables: {
      id: id,
    },
  })) as GraphQLResult<GetCourseQuery>;
  const course = result.data?.getCourse;
  return course;
}

async function getCourseMapForSchedule(
  schedule: Schedule
): Promise<{ [key: string]: Course }> {
  let coursesForSchedule: any = {};

  if (!schedule.items) {
    return coursesForSchedule;
  }

  for (const scheduleItem of schedule.items) {
    const courseId = scheduleItem.course_id;
    if (!courseId) {
      continue;
    }
    coursesForSchedule[courseId] = await getCourseById(courseId);
  }

  return coursesForSchedule;
}

/**
 * Adds a course to a schedule. Assigns a color based the courses already in the schedule.
 *
 * @param schedule The schedule to add a course to.
 * @param id The id of the course to add.
 * @returns The updated schedule.
 */
async function addCourseToSchedule(schedule: Schedule, id: string) {
  // skip adding course if we don't have a schedule to add it to
  if (!schedule?.id || !schedule.items) {
    throw Error("Incomplete schedule");
  }

  const updateScheduleInput: UpdateScheduleInput = {
    id: schedule?.id,
    items: [
      {
        course_id: id,
        selected_crns: [],
        color: getColorByIndex(schedule.items.length),
      },
      ...(schedule.items as ScheduleItemInput[]),
    ],
  };

  const updateResult = (await API.graphql({
    query: mutations.updateSchedule,
    variables: {
      input: updateScheduleInput,
    },
  })) as GraphQLResult<UpdateScheduleMutation>;

  return updateResult.data?.updateSchedule;
}

/**
 * Removes a course from a schedule.
 *
 * @param schedule The schedule to remove the course from.
 * @param id The id of the course to remove from the schedule.
 * @returns The updated schedule.
 */
async function removeCourseFromSchedule(schedule: Schedule, id: string) {
  // skip removing course if we don't have a schedule to remove it from
  if (!schedule?.id || !schedule.items) {
    throw Error("Incomplete schedule");
  }

  const updatedItems = schedule.items.filter((item) => item.course_id !== id);

  const updateScheduleInput: UpdateScheduleInput = {
    id: schedule?.id,
    items: updatedItems as ScheduleItemInput[],
  };

  const updateResult = (await API.graphql({
    query: mutations.updateSchedule,
    variables: {
      input: updateScheduleInput,
    },
  })) as GraphQLResult<UpdateScheduleMutation>;

  return updateResult.data?.updateSchedule;
}

async function updateScheduleItemColor(
  schedule: Schedule,
  courseId: string,
  color: string
) {
  if (!schedule.items) {
    throw Error("Schedule missing items list");
  }

  let items: ScheduleItemInput[] = schedule.items.map((item) => {
    if (item.course_id === undefined || item.selected_crns === undefined) {
      throw Error("Encountered incomplete schedule item");
    }
    return {
      course_id: item.course_id,
      selected_crns: item.selected_crns,
      color: item.course_id === courseId ? color : item.color,
    };
  });

  updateScheduleItems(schedule, items);
}

async function updateScheduleItems(
  schedule: Schedule,
  items: ScheduleItemInput[]
) {
  // the schedule must have an id for the update to work
  if (!schedule.id) {
    throw Error("Schedule must have an ID");
  }

  const updateScheduleInput: UpdateScheduleInput = {
    id: schedule.id,
    items: items,
  };

  const updateResult = (await API.graphql({
    query: mutations.updateSchedule,
    variables: {
      input: updateScheduleInput,
    },
  })) as GraphQLResult<UpdateScheduleMutation>;

  return updateResult.data?.updateSchedule;
}

// TODO: all these schedule update functions have some common code that should be pulled out into a funciton
async function renameSchedule(schedule: Schedule, newName: string) {
  // the schedule must have an id for the update to work
  if (!schedule.id) {
    throw Error("Schedule must have an ID");
  }

  const updateScheduleInput: UpdateScheduleInput = {
    id: schedule.id,
    name: newName,
  };

  const updateResult = (await API.graphql({
    query: mutations.updateSchedule,
    variables: {
      input: updateScheduleInput,
    },
  })) as GraphQLResult<UpdateScheduleMutation>;

  return updateResult.data?.updateSchedule;
}

// TODO: these three schedule subscription helpers have lots of duplicated code
function getScheduleUpdateSubscriptionForUser(
  username: string,
  updateScheduleCallback: (updatedSchedule: Schedule) => void
) {
  const subscription = (API.graphql(
    graphqlOperation(subscriptions.onUpdateSchedule, { owner: username })
  ) as Observable<object>).subscribe({
    next: (value: any) => {
      const updatedSchedule: Schedule = value.value.data.onUpdateSchedule;
      if (updatedSchedule) {
        updateScheduleCallback(updatedSchedule);
      }
    },
    error: (error: any) => {
      console.error("onUpdateSchedule subscription error", error);
    },
  });
  return subscription;
}

function getScheduleCreateSubscriptionForUser(
  username: string,
  createScheduleCallback: (updatedSchedule: Schedule) => void
) {
  const subscription = (API.graphql(
    graphqlOperation(subscriptions.onCreateSchedule, { owner: username })
  ) as Observable<object>).subscribe({
    next: (value: any) => {
      const createdSchedule: Schedule = value.value.data.onCreateSchedule;
      if (createdSchedule) {
        createScheduleCallback(createdSchedule);
      }
    },
    error: (error: any) => {
      console.error("onCreateSchedule subscription error", error);
    },
  });
  return subscription;
}

function getScheduleDeleteSubscriptionForUser(
  username: string,
  deleteScheduleCallback: (updatedSchedule: Schedule) => void
) {
  const subscription = (API.graphql(
    graphqlOperation(subscriptions.onDeleteSchedule, { owner: username })
  ) as Observable<object>).subscribe({
    next: (value: any) => {
      const deletedSchedule: Schedule = value.value.data.onDeleteSchedule;
      if (deletedSchedule) {
        deleteScheduleCallback(deletedSchedule);
      }
    },
    error: (error: any) => {
      console.error("onDeleteSchedule subscription error", error);
    },
  });
  return subscription;
}

export {
  getScheduleById,
  getSchedulesForUser,
  getCourseById,
  getCourseMapForSchedule,
  addCourseToSchedule,
  removeCourseFromSchedule,
  updateScheduleItemColor,
  updateScheduleItems,
  renameSchedule,
  getScheduleUpdateSubscriptionForUser,
  getScheduleCreateSubscriptionForUser,
  getScheduleDeleteSubscriptionForUser,
};
