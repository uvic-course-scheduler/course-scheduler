import { Course, Schedule, ScheduleItem } from "API";
import dedent from "dedent";
import { v4 as uuidv4 } from "uuid";

/**
 * Converts a schedule and associated course data to an iCalendar file.
 * (RFC: https://datatracker.ietf.org/doc/html/rfc5545)
 *
 * @param schedule The schedule to create a calendar file for.
 * @param courses A list of courses appearing in the schedule.
 * @returns A string containing the contents of an iCalendar file for the schedule.
 */
function getICalString(schedule: Schedule, courses: Course[]) {
  let iCalEvents: string[] = [];

  if (schedule.items) {
    for (let scheduleItem of schedule.items) {
      const course: Course | undefined = courses.find((course) => {
        return course.id === scheduleItem.course_id;
      });

      // skip the current course if it doesn't appear in any schedule items
      if (!course) {
        continue;
      }

      // create a calendar event for the current schedule item
      iCalEvents.push(...getICalEvents(scheduleItem, course));
    }
  }

  return dedent`
    BEGIN:VCALENDAR
    VERSION:2.0
    PRODID:UVic Scheduler
    CALSCALE:GREGORIAN
    METHOD:PUBLISH
    ${iCalEvents.join("\n")}
    END:VCALENDAR
  `;
}

function getICalEvents(scheduleItem: ScheduleItem, course: Course): string[] {
  // can't make events without sections in the course
  if (!course.sections) {
    return [];
  }

  // get the selected sections for the course
  const selectedSections = course.sections.filter((section) => {
    if (!scheduleItem.selected_crns || !section || !section.crn) {
      return false;
    }
    return scheduleItem.selected_crns.includes(section?.crn);
  });

  // return all the meeting times the selected sections
  let meetingTimeEvents: string[] = [];
  for (let selectedSection of selectedSections) {
    if (!selectedSection || !selectedSection.meeting_times) {
      continue;
    }

    const courseName = `${course.subject} ${course.course_code}`;
    const sectionName = `${selectedSection.section_type}${selectedSection.section_number}`;

    for (let meetingTime of selectedSection.meeting_times) {
      if (!meetingTime?.rrule) {
        continue;
      }
      const [dtstart, rrule] = meetingTime.rrule.split(" ");

      meetingTimeEvents.push(dedent`
        BEGIN:VEVENT
        DTSTAMP:${dtstart.split(":")[1]}
        ${dtstart}
        ${rrule}
        UID:${uuidv4()}
        DESCRIPTION:${meetingTime?.location}
        SUMMARY:${courseName} ${sectionName}: ${course.course_title}
        END:VEVENT
      `);
    }
  }

  return meetingTimeEvents;
}

/**
 * Download a string as a file.
 * (stolen from: https://stackoverflow.com/questions/3665115/how-to-create-a-file-in-memory-for-user-to-download-but-not-through-server)
 *
 * @param filename The filename of the file to download.
 * @param text The contents of the file.
 */
function downloadStringAsFile(filename: string, text: string) {
  let element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(text)
  );
  element.setAttribute("download", filename);
  element.style.display = "none";
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

export { getICalString, downloadStringAsFile };
